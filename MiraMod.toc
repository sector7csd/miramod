## Interface: 70000
## Title : MiraMod
## Notes: Small debuff scanner with sound notifications
## Notes-deDE: Debuffscanner mit Sound-Meldungen
## Author: Dorijana (aka Miralu), EU-Antonidas (Sector7CSD)
## Version: 1.8.0
## DefaultState: Enabled
## LoadOnDemand: 0
MiraMod.xml
MiraMod.lua
MiraMod_Utils.lua
MiraMod_API.lua
localization.core.lua
localization.de.lua
localization.en.lua
Inis\Wotlk\MM_TheRubySanctum.lua
Inis\Wotlk\MM_IcecrownCitadel.lua
Inis\Cata\MM_TheStoneCore.lua
Inis\Cata\MM_TheVortexPinnacle.lua
Inis\Cata\MM_ShadowfangKeep.lua
Inis\Cata\MM_HallsofOrigination.lua
Inis\Cata\MM_BlackrockCaverns.lua
Inis\Cata\MM_BlackwingDescent.lua
Inis\Cata\MM_TheBastionofTwilight.lua
Inis\Cata\MM_ThroneoftheFourWinds.lua
Inis\Cata\MM_Firelands.lua
Inis\Cata\MM_DragonSoul.lua
Inis\MoP\MM_MogushanVaults.lua
Inis\MoP\MM_HeartofFear.lua
Inis\MoP\MM_SiegeofOrgrimmar.lua
Inis\WoD\MM_Highmaul.lua
Inis\WoD\MM_BlackrockFoundry.lua
Inis\WoD\MM_HellfireCitadel.lua
Inis\Legion\MM_CourtofStars.lua
Inis\Legion\MM_AssaultonVioletHold.lua
