MiraModInstance = {};
MiraModInstance.__index = MiraModInstance;

function MiraModInstance.create(name)

	local instance = {};
	setmetatable(instance, MiraModInstance);

	instance.name 				= name;
	instance.debuffs 			= {};			-- List of debuffs
												-- elements: name			Name of the debuff
												--			 state			True = active, false = not active
												--			 addSound		Played soundfile when debuff is detected
												--			 addMsg			Displayed text message
												--			 removeSound	Played soundfile when debuff is removed
												--			 removeMsg		Displayed text message
												--			 scanInGroup	True if this debuff has to be searched in the group, to find special debuffs like blackout
												--			 addYell		Text which is yelled when debuff is detected
												--			 removeYell		Text which is yelled when debuff is removed
												--			 stackCount		Warning will be only fired when the debuff reaches this or more stack count
												--			 triggersCount	True, if this debuffs causes an countdown
												--			 countdown		Used for countdown
												--			 countdownPos	Position for countdown

	instance.combat				= {};			-- List of combat events
												-- elements: source			caster
												-- 			 actionType		can be SPELL_CAST_SUCCESS, SPELL_AURA_APPLIED, SPELL_AURA_REMOVED
												--			 spellId		Id of the spell, if known
												--			 spellName		Name of the spell
												--			 textMsg		Text message which is displayed
												--			 func			Function that is called
												--			 stackCount		Stackcount
												--			 target			Target of the spell

												-- Called when ...
	instance.onYell				= nil;			-- ... a NPC is yelling
	instance.onRaidTargetUpdate = nil;			-- ... raid targets are updated
	instance.onStartEncounter 	= nil;			-- ... an encounter starts
	instance.onEndEncounter		= nil;			-- ... an encounter ends

	return instance;

end

function MiraMod_AddInstance(name, initFunction)

	local locName = MiraMod_GetTranslation(name);

	local ini = MiraModInstance.create(locName);
	initFunction(ini);

	local debuffCount = table.getn(ini.debuffs);
	local combatCount = table.getn(ini.combat);
	MiraMod_Write("'" .. ini.name .. "' " .. MiraMod_GetTranslation("prepared") .. ", Debuffs: " .. debuffCount .. ", Combat: " .. combatCount, 0.5, 0.5, 1.0);

	MiraModInstances[locName] = ini;
end

function MiraModInstance:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown, announceStackCount)

	local entry = {};

	if(scanInGroup == nil) then
		scanInGroup = false;
	end

	if(stackCount == nil) then
		stackCount = 0;
	end

	if(tiggerCountdown == nil) then
		tiggerCountdown = false;
	end

	if(countDown == nil) then
		countDown = 0;
	end

	if(announceStackCount == nil) then
		announceStackCount = false;
	end

	entry["state"]			= false;
	entry["countdownPos"]	= 0;

	entry["name"] 				= MiraMod_GetTranslation(name);
	entry["addSound"]			= addSound;
	entry["addMsg"]				= MiraMod_GetTranslation(addTextMsg);
	entry["removeSound"]		= removeSound;
	entry["removeMsg"]			= MiraMod_GetTranslation(removeMsg);
	entry["scanInGroup"]		= scanInGroup;
	entry["addYell"]			= MiraMod_GetTranslation(addYell);
	entry["removeYell"]			= MiraMod_GetTranslation(removeYell);
	entry["stackCount"]			= stackCount;
	entry["triggersCount"]		= tiggerCountdown;
	entry["countdown"]			= countDown;
	entry["announceStackCount"] = announceStackCount;

	table.insert(self.debuffs, entry);

end

function MiraModInstance:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)

	local entry = {};

	if(stackCount == nil) then
		stackCount = 0;
	end

	entry["source"] 	= MiraMod_GetTranslation(source);
	entry["actionType"]	= actionType;
	entry["spellId"] 	= spellId;
	entry["spellName"] 	= MiraMod_GetTranslation(spellName);
	entry["sound"]		= sound;
	entry["textMsg"] 	= MiraMod_GetTranslation(textMsg);
	entry["func"] 		= func;
	entry["stackCount"] = stackCount;
	entry["target"] 	= MiraMod_GetTranslation(target);

	table.insert(self.combat, entry);

end

function MiraMod_AddSpecialTimer(EndTime, Msg, Snd, Function, Name)

	local entry = {};
	entry["endTime"] = tonumber(EndTime);
	entry["msg"]	 = Msg;
	entry["snd"]	 = Snd;
	entry["func"]	 = Function;
	entry["name"]	 = Name;
	table.insert(MiraModTimers, entry);

end

function MiraMod_CancelSpecialTimer(Name)

	for za=TimerSize,1,-1 do

		local entry   = MiraModTimers[za];

		if(entry["name"] ~= nil) then

			if(entry["name"] == Name) then

				table.remove(MiraModTimers, za);
				return;

			end

		end

	end

end
