local _, L = ...;

local activeInstance = nil;

function MiraMod_OnLoad()

	-- Register events
	MiraModEventTable				= {};
	MiraModEventTable["COMBAT_LOG_EVENT_UNFILTERED"]	= MiraMod_CombatLog_Event;
	MiraModEventTable["VARIABLES_LOADED"]				= MiraMod_OnVariablesLoaded;
	MiraModEventTable["PLAYER_ENTERING_WORLD"]			= MiraMod_ZoneCheck;
	MiraModEventTable["ENCOUNTER_START"]				= MiraMod_Encounter_Start;
	MiraModEventTable["ENCOUNTER_END"]					= MiraMod_Encounter_End;
	MiraModEventTable["RESURRECT_REQUEST"]				= MiraMod_Resurrect_Request;
	MiraModEventTable["READY_CHECK"]					= MiraMod_ReadyCheck;
	MiraModEventTable["CHAT_MSG_MONSTER_YELL"]			= MiraMod_Monster_Yell;
	MiraModEventTable["RAID_TARGET_UPDATE"]				= MiraMod_RaidTargetUpdate;

	for key,value in pairs(MiraModEventTable) do
		MiraMod:RegisterEvent(key);
	end

	SlashCmdList["MiraMod"] = MiraMod_OnCommand;
	SLASH_MiraMod1 = "/Miramod";
	SLASH_MiraMod2 = "/miramod";

	-- Init global variables
	UpdateTimeInterval 				= 0.5;
	TimeSinceLastUpdate				= 0;
	MiraModTextDisplay 				= nil;
	SoundDir 						= nil;
	nextraid_symbol 				= 8;

	MiraModInstances				= {};	-- List of instances
											-- key: 	 instance name
											-- elements: active			Boolean that contains the info if the init function has been called yet
											--			 initFunc		Function that would add the debuffs/combat entries to the scanner
											--							When the instance has been entered

	MiraModTimers					= {};	-- List of active timers
											-- elements: endTime		Contains the timestamp when the timer rans out
											-- 			 msg			Message that is displayed
											--			 snd			Sound that is played
											--			 func			Function that is called
end

function MiraMod_PlaySound(file)

	-- MiraMod_Write("PlaySound: " .. SoundDir .. file);
	PlaySoundFile(SoundDir .. file .. ".ogg", "Master");

end

function MiraMod_PlayCommonSound(file)

	local fullname = "Interface\\AddOns\\MiraMod\\Sounds\\common\\" .. file .. ".ogg";
	PlaySoundFile(fullname, "Master");

end

function MiraMod_GetTranslation(id)

	return L[id];

end

function MiraMod_OnCommand(arg)

	local str = L[arg];
	MiraMod_Write("L["..arg.."] = "..str);

	if(arg == "combat") then

		if(activeInstance == nil) then

			MiraMod_Write("activeInstance is nil");
			return;

		end

		local combat = activeInstance.combat;
		local CombatCount = table.getn(combat);

		for za=1,CombatCount do

			local entry = combat[za];

			src 	= MiraMod_CheckParam(entry["source"]);
			spell 	= MiraMod_CheckParam(entry["spellName"]);
			id		= MiraMod_CheckParam(entry["spellId"]);
			act 	= MiraMod_CheckParam(entry["actionType"]);
			dest 	= MiraMod_CheckParam(entry["target"]);

			local msg = "Source["..za.."] = "..src;

			if(spell == "-") then

				msg = msg .. ", spellId = " .. id;

			else

				msg = msg .. ", spell = " .. spell;

			end

			msg = msg .. ", Action = "..act..", Target: "..dest;
			MiraMod_Write(msg);

		end
	end

	if(arg == "debuff") then

		if(activeInstance == nil) then

			MiraMod_Write("activeInstance is nil");
			return;

		end

		local debuffs = activeInstance.debuffs;

		local DebuffCount = table.getn(debuffs);

		for za=1,DebuffCount do

			if(debuffs[za] ~= nil) then

				local entry = debuffs[za];
				local msg = "Name[" .. za .. "] = " .. entry["name"];
				MiraMod_Write(msg);

			end

		end

	end

	if(arg == "ini") then

		for key,value in pairs(MiraModInstances) do
			MiraMod_Write("Ini: " .. key);
		end

	end

end

function MiraMod_TestTimer()

	MiraMod_AddSpecialTimer(time() + 5, "Test Timer ran out", "gouge", MiraMod_SetRaidSymbol);

end

function MiraMod_OnVariablesLoaded()

	MiraModTextDisplay = _G["MiraModTextDisplay"];

	MiraModTextDisplay:Show();
	MiraModTextDisplay:SetFadeDuration(1);
	MiraModTextDisplay:SetTimeVisible(2);

	if(SoundDir == nil) then
		SoundDir = "Interface\\AddOns\\MiraMod\\Sounds\\en";
	end

	SoundDir = SoundDir .. "\\";

	-- Wotlk
	MiraMod_AddInstance("IcecrownCitadel", 		MiraMod_IcecrownCitadel_Init);
	MiraMod_AddInstance("TheRubySanctum", 		MiraMod_RubySanctum_Init);

	-- Cata
	MiraMod_AddInstance("BlackrockCaverns",     MiraMod_BlackrockCaverns_Init);
	MiraMod_AddInstance("TheStonecore",			MiraMod_TheStonecore_Init);
	MiraMod_AddInstance("TheVortexPinnacle",	MiraMod_TheVortexPinnacle_Init);
	MiraMod_AddInstance("ShadowfangKeep",		MiraMod_ShadowfangKeep_Init);
	MiraMod_AddInstance("HallsofOrigination",	MiraMod_HallsofOrigination_Init);
	MiraMod_AddInstance("BlackwingDescent", 	MiraMod_BlackwingDescent_Init);
	MiraMod_AddInstance("TheBastionofTwilight", MiraMod_TheBastionofTwilight_Init);
	MiraMod_AddInstance("ThroneoftheFourWinds", MiraMod_FourWinds_Init);
	MiraMod_AddInstance("Firelands", 			MiraMod_Firelands_Init);
	MiraMod_AddInstance("DragonSoul", 			MiraMod_DragonSoul_Init);

	-- MoP
	MiraMod_AddInstance("MogushanVaults", 		MiraMod_MogushanVaults_Init);
	MiraMod_AddInstance("HeartofFear", 			MiraMod_HeartofFear_Init);
	MiraMod_AddInstance("SiegeofOrgrimmar", 	MiraMod_SiegeofOrgrimmar_Init);

	-- WoD
	MiraMod_AddInstance("Highmaul", 			MiraMod_Highmaul_Init);
	MiraMod_AddInstance("BlackrockFoundry", 	MiraMod_BlackrockFoundry_Init);
	MiraMod_AddInstance("HellfireCitadel",		MiraMod_HellfireCitadel_Init);

	-- Legion
	MiraMod_AddInstance("CourtofStars",			MiraMod_CourtofStars_Init);
	MiraMod_AddInstance("AssaultonVioletHold",	MiraMod_AssaultonVioletHold_Init);

	-- Test
	-- ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)
	-- ini:addDebuff("Weakened Soul", "seuche", "Weakened Soul Get", "verbindungweg", "Weakened Soul Rem", false, "Chogall_Corruption_Sickness_Yell_Add", nil, 0, true, 13);
	-- ini:addCombat("Dorijana", "SPELL_AURA_APPLIED", 0, "Power Word Fortitude", "lauf", "Power Word Fortitude", MiraMod_TestTimer);

	-- Say hello
	if(DEFAULT_CHAT_FRAME ~= nil) then
		MiraMod_Write(L["StartUpMsg"], 0.5, 0.5, 1.0);
	else
		MiraModTextDisplay:AddMessage(L["StartUpMsg"], 0.5, 0.5, 1.0);
	end

end

function MiraMod_SetRaidSymbol(Target)

	if(Target ~= nil) then

		local icon = GetRaidTargetIndex(Target);

		if(icon ~= nextraid_symbol) then
			SetRaidTargetIcon(Target, nextraid_symbol);
		end

		if(nextraid_symbol == 8) then

			MiraMod_AddSpecialTimer(time() + 1, nil, nil, MiraMod_OnResetRaidSymbol);

		end
	end

	nextraid_symbol = nextraid_symbol - 1;

	if(nextraid_symbol<0) then
		nextraid_symbol = 8;
	end

end

function MiraMod_RemoveRaidSymbol(Target)

	if(Target ~= nil) then

		local icon = GetRaidTargetIndex(Target);
		if(icon ~= nil) then

			SetRaidTargetIcon(Target, icon);

		end

	end

end

function MiraMod_OnResetRaidSymbol()

	nextraid_symbol = 8;

end

function MiraMod_OnUpdate(self, elapsed)

	if(activeInstance==nil) then
		return;
	end

	if(MiraModTextDisplay) then

		TimeSinceLastUpdate = TimeSinceLastUpdate + elapsed;

		if (TimeSinceLastUpdate > UpdateTimeInterval) then

			local numplayers = GetNumGroupMembers();

			local debuffs = activeInstance.debuffs;

			local za = 0;
			local debuffCount = table.getn(debuffs);

			for za=1, debuffCount do

				local Debuff, Rank, Icon, Count, DebuffType, Duration, expirationTime;

				if(debuffs[za]["scanInGroup"] == false) then

					if(debuffs[za]["name"] ~= nil) then

						Debuff, Rank, Icon, Count, DebuffType, Duration, expirationTime = UnitDebuff("player", debuffs[za]["name"]);

						if(Debuff == nil) then
							Debuff, Rank, Icon, Count, DebuffType, Duration, expirationTime = UnitBuff("player", debuffs[za]["name"]);
						end

					end

				else

					for zb=1,numplayers do

						local name = GetRaidRosterInfo(zb);

						if(name ~= nil) then
							Debuff, Rank, Icon, Count = UnitDebuff(name, debuffs[za]["name"]);
							if(Debuff ~= nil) then
								break
							end
						end

					end

				end

				if(Debuff) then

					if(not debuffs[za]["state"] and (debuffs[za]["stackCount"]==0 or Count >= debuffs[za]["stackCount"])) then

						if(debuffs[za]["addSound"] ~= nil) then
							MiraMod_PlaySound(debuffs[za]["addSound"]);
						end

						if(debuffs[za]["addMsg"] ~= nil) then
							MiraModTextDisplay:AddMessage(debuffs[za]["addMsg"], 1.0, 0.32, 0.66);
						end

						if(debuffs[za]["addYell"] ~= nil) then
							SendChatMessage(debuffs[za]["addYell"], "YELL", nil, 0);
						end

						debuffs[za]["state"] = true;

						if(debuffs[za]["triggersCount"] == true) then
							debuffs[za]["countdown"] = math.floor(expirationTime - GetTime()) - 1;
						end

						if(debuffs[za]["countdown"] > 0) then
							debuffs[za]["countdownPos"] = debuffs[za]["countdown"];
						end

						if(debuffs[za]["announceStackCount"] == true) then
							MiraMod_PlaySound(Count);
							MiraModTextDisplay:AddMessage(">> " .. Count .. " <<", 1.0, 0.32, 0.66);
						end

					end
				end

				if(debuffs[za]["countdownPos"] ~= nil) then

					if(debuffs[za]["countdownPos"]>UpdateTimeInterval) then

						local countPos, fraction = math.modf(debuffs[za]["countdownPos"]);

						local countPos = math.floor(debuffs[za]["countdownPos"]);
						if(countPos<10 and countPos > 0 and fraction == 0) then
							MiraMod_PlaySound(countPos);
						end

						debuffs[za]["countdownPos"] = debuffs[za]["countdownPos"] - UpdateTimeInterval;

					end

				end

				if(not Debuff and debuffs[za]["state"]==true) then

					debuffs[za]["state"] = false;

					if(debuffs[za]["removeMsg"] ~= nil) then
						MiraModTextDisplay:AddMessage(debuffs[za]["removeMsg"], 0.32, 1.00, 0.32);
					end

					if(debuffs[za]["removeSound"] ~= nil) then
						MiraMod_PlaySound(debuffs[za]["removeSound"]);
					end

					if(debuffs[za]["removeYell"] ~= nil) then
						SendChatMessage(debuffs[za]["removeYell"], "YELL", nil, 0);
					end

					if(debuffs[za]["countdownPos"]>0) then
						debuffs[za]["countdownPos"] = 0;
					end

				end

			end

			local TimerSize 	= #MiraModTimers;
			local CurrentTime 	= time();

			for za=TimerSize,1,-1 do

				local entry   = MiraModTimers[za];
				local endTime = entry["endTime"];

				if(endTime <= CurrentTime) then

					-- Timer ran out
					local Msg = entry["msg"];
					local Snd = entry["snd"];
					local Func= entry["func"];

					if(Msg ~= nil) then
						MiraModTextDisplay:AddMessage(Msg, 1.0, 1.0, 0.55);
					end

					if(Snd ~= nil) then
						MiraMod_PlaySound(Snd);
					end

					if(Func ~= nil) then
						Func(activeInstance);
					end

					table.remove(MiraModTimers, za);

				end

			end

			TimeSinceLastUpdate = 0;

		end

	end

end

function MiraMod_ZoneCheck()

	local inInstance = IsInInstance();

	if(inInstance) then

		local name = GetInstanceInfo();

		MiraMod_Write("InstanceName: " .. name);

		local entry = MiraModInstances[name];
		if(entry ~= nil) then

			activeInstance = entry;
			MiraMod_Write(activeInstance.name .. " " .. L["active"], 0.5, 0.5, 1.0);

		else

			activeInstance = nil;

		end

	else

		activeInstance = nil;

	end
end

function MiraMod_CombatLog_Event(arg1, action, arg3, srcGuid, src, arg6, arg7, destGuid, dest, arg10, arg11, spellId, spellName, arg14, arg15, stackCount, arg17, arg18)

	if(activeInstance==nil) then
		return;
	end

	local za = 0;
	local found = false;

	local combat = activeInstance.combat;

	local CombatCount = table.getn(combat);

	for za=1, CombatCount do

		if(action == combat[za]["actionType"]) then

			local spellCheck 	= (combat[za]["spellId"] 	== spellId 		or  combat[za]["spellName"]	== spellName);
			local sourceCheck 	= combat[za]["source"] 		== src 			or  combat[za]["source"] 	== nil;
			local diedCheck		= combat[za]["actionType"] 	== "UNIT_DIED" 	and combat[za]["source"] 	== dest;

			if (spellCheck and sourceCheck) or (diedCheck) then

				if(combat[za]["stackCount"] == 0 or combat[za]["stackCount"] == stackCount) then

					if(combat[za]["target"] == nil or combat[za]["target"] == dest) then

						found = true;

						if(combat[za]["sound"] ~= nil) then
							MiraMod_PlaySound(combat[za]["sound"]);
						end

						if(combat[za]["textMsg"] ~= nil) then
							MiraModTextDisplay:AddMessage(combat[za]["textMsg"], 1.0, 0.32, 0.66);
						end

						if(combat[za]["func"] ~= nil) then
							local dest = combat[za]["func"];
							dest(activeInstance, action, srcGuid, src, destGuid, dest, spellId, spellName, stackCount);
						end

						break;

					end

				end

			end

		end
	end
end

function MiraMod_Encounter_Start(arg1, arg2, arg3, arg4)

	MiraMod_Write("Start encounter: " .. arg2);

	if(activeInstance == nil) then
		return;
	end

	if(activeInstance.onStartEncounter ~= nil) then
		activeInstance.onStartEncounter(arg1, arg2, arg3, arg4, activeInstance);
	end

end

function MiraMod_Encounter_End(arg1, arg2, arg3, arg4, arg5)

	local result = "wipe";

	if arg5 == 1 then

		result = "success";

		--MiraMod_PlayCommonSound("FF1");

	end

	MiraMod_Write("End encounter: " .. arg2.. ", result: " .. result);
	MiraMod_ClearTable(MiraModTimers);

	if(activeInstance == nil) then
		return;
	end

	if(activeInstance.onEndEncounter ~= nil) then
		activeInstance.onEndEncounter(arg1, arg2, arg3, arg4, arg5, activeInstance);
	end

end

function MiraMod_Monster_Yell(arg1, arg2)

	if(activeInstance == nil) then
		return;
	end

	if(activeInstance.onYell ~= nil) then
		activeInstance.onYell(arg1, arg2, activeInstance);
	end

end

function MiraMod_RaidTargetUpdate()

	if(activeInstance == nil) then
		return;
	end

	if(activeInstance.onRaidTargetUpdate ~= nil) then
		activeInstance.onRaidTargetUpdate(activeInstance);
	end

end

function MiraMod_Resurrect_Request()

	MiraMod_PlayCommonSound("extralife");

end

function MiraMod_ReadyCheck()

	MiraMod_PlayCommonSound("start");

end

function MiraMod_OnEvent(self, event, ...)

	local arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17, arg18 = select(1, ...)

	if MiraModTextDisplay == nil then
		return;
	end

	func = MiraModEventTable[event];
	if(func ~= nil) then

		func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17, arg18);

	end

end
