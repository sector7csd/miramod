local _, L = ...;
if GetLocale() == "deDE" then

	SoundDir = "Interface\\AddOns\\MiraMod\\Sounds\\de";

	-- Common messages
	L["StartUpMsg"]	 							= "MiraMod geladen und bereit...";
	L["Phase2"]									= ">> Phase 2 <<";
	L["Phase3"]									= ">> Phase 3 <<";
	L["AirPhase"]								= ">> Flugphase <<";
	L["AirPhase_Soon"]							= ">> Flugphase bald <<";
	L["NextPhaseSoon"]							= ">> N\195\164chste Phase bald <<";
	L["10m_Range"]								= "10 m Abstand halten";
	L["prepared"]								= "vorbereitet";
	L["active"]									= "aktiv";

	L["NewAdd"]									= ">> Neues Add <<";
	L["AddDown"]								= ">> Add down <<";
	L["NewAddSoon"]								= ">> Neues Add bald <<";

	L["Shadowvortex"]							= "Schattenvortex";
	L["Shadowvortex_Add"]						= ">> Schattenvortex <<";
	L["Shadowvortex_Rem"]						= ">> Schattenvortex weg <<";
	L["BubbleBound"]							= "Kugelgef\195\164ngnis";
	L["BubbleBound_Add"]						= ">> Kugelgef\195\164ngnis <<";
	L["BubbleBound_Rem"]						= ">> Kugelgef\195\164ngnis weg <<";
	L["BubbleBound_Yell_Add"]					= ">> Kugelgef\195\164ngnis auf mir ! <<";
	L["BubbleBound_Yell_Rem"]					= "<< Kugelgef\195\164ngnis entfernt >>";

	-- Test
	L["Weakened Soul"]							= "Geschw\195\164chte Seele";
	L["Weakened Soul Get"]						= ">> Geschw\195\164chte Seele <<";
	L["Weakened Soul Rem"] 						= ">> Geschw\195\164chte Seele weg <<";
	L["Power Word Fortitude"]					= "Machtwort: Seelenst\195\164rke";

	-- Instances
	L["BlackwingDescent"]						= "Pechschwingenabstieg";
	L["TheBastionofTwilight"]					= "Die Bastion des Zwielichts";
	L["IcecrownCitadel"]						= "Eiskronenzitadelle";
	L["Firelands"]								= "Feuerlande";
	L["MogushanVaults"]							= "Mogu'shangew\195\182lbe";
	L["ThroneoftheFourWinds"]					= "Thron der Vier Winde";
	L["HeartofFear"]							= "Das Herz der Angst";
	L["DragonSoul"]								= "Drachenseele";
	L["TheRubySanctum"]							= "Das Rubinsanktum";
	L["SiegeofOrgrimmar"]						= "Schlacht um Orgrimmar";
	L["Highmaul"]								= "Hochfels";
	L["BlackrockFoundry"]						= "Schwarzfelsgie\195\159erei";
	L["BlackrockCaverns"]						= "Schwarzfelsh\195\182hlen";
	L["ShadowfangKeep"]							= "Burg Schattenfang";
	L["TheVortexPinnacle"]						= "Der Vortexgipfel";
	L["TheStonecore"]							= "Der Steinerne Kern";
	L["HallsofOrigination"]						= "Hallen des Ursprungs";
	L["HellfireCitadel"]						= "H\195\182llenfeuerzitadelle";
	L["CourtofStars"]							= "Der Hof der Sterne";
	L["AssaultonVioletHold"]					= "Sturm auf die Violette Festung";

	-- Sha of Pride
	L["ShaofPride"]								= "Sha des Stolzes";
	L["ShaofPride_GiftoftheTitans"]				= "Gabe der Titanen";
	L["ShaofPride_GiftoftheTitans_Add"]			= ">> Gabe der Titanen <<";
	L["ShaofPride_GiftoftheTitans_Rem"]			= ">> Gabe der Titanen ausgelaufen <<";
	L["ShaofPride_PoweroftheTitans"]			= "Macht der Titanen";
	L["ShaofPride_PoweroftheTitans_Add"]		= ">> Macht der Titanen <<";
	L["ShaofPride_PoweroftheTitans_Rem"]		= ">> Macht der Titanen ausgelaufen <<";
	L["ShaofPride_WeakenedResolve"]				= "Schwindende Entschlossenheit";
	L["ShaofPride_WeakenedResolve_Add"]			= ">> Riss Debuff <<";
	L["ShaofPride_WeakenedResolve_Rem"]			= ">> Riss Debuff weg <<";
	L["ShaofPride_WeakenedResolve_Yell_Rem"]	= ">> Riss Debuff ausgelaufen <<";
	L["ShaofPride_SwellingPride"]				= "Wachsender Stolz";
	L["ShaofPride_SwellingPride_Soon"]			= ">> Wachsender Stolz bald <<";
	L["ShaofPride_Projection"]					= "Projektion";
	L["ShaofPride_Projection_Add"]				= ">> Projektion <<";
	L["ShaofPride_Projection_Rem"]				= ">> Projektion weg <<";
	L["ShaofPride_Banishment"]					= "Bannung";
	L["ShaofPride_Banishment_Add"]				= ">> Bannung <<";
	L["ShaofPride_Banishment_Rem"]				= ">> Bannung weg <<";

	-- Iron Juggernaut
	L["IronJuggernaut"]							= "Eiserner Koloss";
	L["IronJuggernaut_SeismicActivity"]			= "Seismische Aktivit\195\164t";
	L["IronJuggernaut_ShockPulse"]				= "Schockimpuls";
	L["IronJuggernaut_ShockPulse_Msg"]			= ">> Schockimpuls in 3 seks <<";
	L["IronJuggernaut_CutterLaser"]				= "Laserskalpell";
	L["IronJuggernaut_CutterLaserTarget"]		= "Laserskalpellziel";
	L["IronJuggernaut_CutterLaserTarget_Msg"]	= ">> Laserskalpellziel <<";

	-- Norushen
	L["Norushen"]								= "Norushen";
	L["Norushen_StopIt"]						= "Ihr... m\195\188sst... es... aufhalten...";

	-- Fallen Protectors
	L["Fallen_Protectors_He_Softfoot"]			= "Hee Samtfu\195\159";
	L["Fallen_Protectors_Gouge"]				= "Solarplexus";
	L["Fallen_Protectors_Gouge_Msg"]			= ">> Solarplexus <<";

	-- ICC Stuff
	L["Necrotic Plague"] 						= "Nekrotische Seuche";
	L["Plague"]									= ">> Seuche <<";

	-- Sindragosa
	L["Sindragosa"]								= "Sindragosa";
	L["Sindragosa_Pull"]						= "Ihr seid Narren, euch hierher zu wagen. Der eisige Wind Nordends wird eure Seelen verschlingen!";
	L["Sindragosa_AirPhase"]					= "Euer Vormarsch endet hier! Keiner wird \195\188berleben!";
	L["Sindragosa_Phase2"]						= "F\195\188hlt die grenzenlose Macht meines Meisters, und verzweifelt!";
	L["Sindragosa_GoodLuck"]					= "Sindragosa gepullt - Viel Gl\195\188ck !";
	L["Sindragosa_Zone"]						= "Der Hort der Frostk\195\182nigin";
	L["Sindragosa_Front_Left"]					= "VORNE LINKS";
	L["Sindragosa_Front_Right"]					= "VORNE RECHTS";
	L["Sindragosa_Center"]						= "MITTE";
	L["Sindragosa_Rear_Left"]					= "HINTEN LINKS";
	L["Sindragosa_Rear_Right"]					= "HINTEN RECHTS";
	L["Sindragosa_FrostBeacon"]					= "Frostleuchtfeuer";
	L["Sindragosa_Unleashed_Magic"]				= "Entfesselte Magie";
	L["Sindragosa_Unleashed_Magic_Add"]			= ">> Entfesselte Magie <<";
	L["Sindragosa_Unleashed_Magic_Rem"]			= ">> Entfesselte Magie weg <<";

	-- Prof Putricide
	L["Putricide_Plague_Sickness"]				= "Seuchenkrankheit";
	L["Putricide_Plague_Sickness_Add"]			= ">> Krankheit <<";
	L["Putricide_Plague_Sickness_Rem"]			= ">> Krankheit weg <<";
	L["Putricide_Unbound_Plague"]				= "Entfesselte Seuche";
	L["Putricide_Unbound_Plague_Add"]			= ">> Seuche <<";
	L["Putricide_Unbound_Plague_Rem"]			= ">> Seuche weg <<";

	-- Blood Prince Council
	L["BloodCouncil_Shadow_Prison"]				= "Schattengef\195\164ngnis";
	L["BloodCouncil_Shadow_Prison_Rem"]			= ">> Schattengef\195\164ngnis weg <<";

	-- Halion
	L["Halion_Fiery_Combustion"]				= "Feurige Ein\195\164scherung";
	L["Halion_Fiery_Combustion_Add"]			= ">> Feurige Ein\195\164scherung <<";
	L["Halion_Fiery_Combustion_Rem"]			= ">> Feurige Ein\195\164scherung weg <<";
	L["Halion_Soul_Consumption"]				= "Seelenverzehrung";
	L["Halion_Soul_Consumption_Add"]			= ">> Seelenverzehrung <<";
	L["Halion_Soul_Consumption_Rem"]			= ">> Seelenverzehrung weg <<";

	-- Ozruk
	L["Ozruk"]									= "Ozruk";
    L["Ozruk_Reflect"]							= "Ihr werdet an meinem K\195\182rper zerschellen. Sp\195\188rt der Erde Macht!";
	L["Ozruk_Reflect_Msg"]						= ">> Dot's setzen <<";

	-- Maloriak
	L["Maloriak"]								= "Maloriak";
	L["Maloriak_Blue_Phase"]					= "Muss rausfinden, wie die sterbliche H\195\188lle auf extreme Temperaturen reagiert... F\195\156R DIE FORSCHUNG!";
	L["Maloriak_Blue_Phase_Msg"]				= ">> Blaue Phase <<";
	L["Maloriak_Green_Phase"]					= "Etwas instabil vielleicht, doch keine Forschung ohne Risiko!";
	L["Maloriak_Green_Phase_Msg"]				= ">> Gr\195\188ne Phase <<";
	L["Maloriak_Red_Phase"]						= "Vermischen, r\195\188hren, erhitzen...";
	L["Maloriak_Red_Phase_Msg"]					= ">> Rote Phase <<";
	L["Maloriak_Low_HP_1"]						= "Mein Hirn und ihre Kraft! Meine Gesch\195\182pfe, verschlingt sie!";
	L["Maloriak_Low_HP_2"]						= "Zu fr\195\188h, doch es muss sein... der Test ist notwendig! Stellt Euch meinen Experimenten!";
	L["Maloriak_Slime"]							= "Entkr\195\164ftender Schleim";
	L["Maloriak_Slime_Add"]						= ">> Entkr\195\164ftender Schleim <<";
	L["Maloriak_Slime_Rem"]						= ">> Entkr\195\164ftender Schleim weg <<";
	L["Maloriak_Freeze"]						= "Blitzeis";
	L["Maloriak_Freeze_Add"]					= ">> Blitzeis <<";
	L["Maloriak_Freeze_Rem"]					= ">> Blitzeis weg <<";
	L["Maloriak_Freeze_Yell_Add"]				= ">> Blitzeis auf mir ! <<";
	L["Maloriak_Freeze_Yell_Rem"]				= "<< Blitzeis entfernt >>";
	L["Maloriak_Flames"]						= "Verzehrende Flammen";
	L["Maloriak_Flames_Add"]					= ">> Verzehrende Flammen <<";
	L["Maloriak_Flames_Rem"]					= ">> Verzehrende Flammen weg <<";
	L["Maloriak_Storm"]							= "Arkaner Sturm";
	L["Maloriak_Storm_Msg"]						= ">> Arkaner Sturm <<";

	-- Chimaeron
	L["Chimaeron"]								= "Schimaeron";
	L["Chimaeron_Feud"]							= "Fehde";
	L["Chimaeron_Feud_Msg"]						= ">> Fehde, zusammenlaufen <<";
	L["Chimaeron_DoubleAttack"]					= "Doppelangriff";
	L["Chimaeron_DoubleAttack_Msg"]				= ">> Doppelangriff <<";
	L["Chimaeron_Mortality"]					= "Sterblichkeit";

	-- Atramedes
	L["Atramedes"]								= "Atramedes";
	L["Atramedes_Pull"]							= "F\195\188r Euch brauche ich meine Augen nicht. Euer plumpes Getrampel und Euer Gestank verraten Euch!";
	L["Atramedes_Flame"]						= "Keiner entkommt der sengenden Flamme!";
	L["Atramedes_AirPhase"]						= "Ja, lauft! Jeder Schritt l\195\164sst Euer Herz st\195\164rker klopfen. Laut und heftig... ohrenbet\195\164ubend. Es gibt kein Entkommen!";
	L["Atramedes_Flame_Soon_Msg"]				= ">> Sengende Flamme bald <<";
	L["Atramedes_Flame_Msg"]					= ">> Sengende Flamme <<";
	L["Atramedes_Flame"]						= "Sengende Flamme";
	L["Atramedes_Execution_Sentence"]			= "Todesurteil";
	L["Atramedes_Execution_Sentence_Add"]		= ">> Todesurteil <<";
	L["Atramedes_Execution_Sentence_Rem"]		= ">> Todesurteil weg <<";
	L["Atramedes_Execution_Sentence_Yell_Add"]	= ">> Todesurteil auf mir ! <<";
	L["Atramedes_Execution_Sentence_Yell_Rem"]	= "<< Todesurteil entfernt >>";
	L["Atramedes_Obnoxious_Fiend"]				= "Nerviges Scheusal";
	L["Atramedes_Obnoxious_Fiend_PhaseShift"]	= "Phasenverschiebung";

	-- Halfus
	L["Halfus_DarkPool"]						= "Dunkler Teich";
	L["Halfus_DarkPool_Add"]					= ">> Dunkler Teich <<";
	L["Halfus_DarkPool_Rem"]					= ">> Dunkler Teich weg <<";

	-- Valiona
	L["Valiona"]								= "Valiona";
	L["Valiona_Flames"]							= "Verschlingende Flammen";
	L["Valiona_Flames_Msg"]						= ">> Verschlingende Flammen <<";
	L["Valiona_Twilight_Meteorite"]				= "Zwielichtmeteorit";
	L["Valiona_Twilight_Meteorite_Add"]			= ">> Meteor, zusammen laufen <<";
	L["Valiona_Twilight_Meteorite_Rem"]			= ">> Meteor weg, verteilen <<";
	L["Valiona_Blackout"]						= "Blackout";
	L["Valiona_Blackout_Add"]					= ">> Blackout, zusammen laufen <<";
	L["Valiona_Blackout_Rem"]					= ">> Blackout weg, verteilen <<";
	L["Valiona_Engulfing_Magic"]				= "Einh\195\188llende Magie";
	L["Valiona_Engulfing_Magic_Add"]			= ">> Einh\195\188llende Magie <<";
	L["Valiona_Engulfing_Magic_Rem"]			= ">> Einh\195\188llende Magie weg <<";

	-- Arion
	L["Arion"]									= "Arion";
	L["Arion_Phase2"]							= "Genug der Spielereien!";
	L["Arion_Thundershock"]						= "Donnerschock";
	L["Arion_Winds"]							= "Wirbelnde Winde";
	L["Arion_Winds_Add"]						= ">> Luftbuff <<";
	L["Arion_Lightning_Rod"]					= "Blitzableiter";
	L["Arion_Lightning_Rod_Add"]				= ">> Blitzableiter <<";
	L["Arion_Lightning_Rod_Rem"]				= ">> Blitzableiter weg <<";

	-- Terrastra
	L["Terrastra"]								= "Terrastra";
	L["Terrastra_Quake"]						= "Beben";
	L["Terrastra_Grounded"]						= "Geerdet";
	L["Terrastra_Grounded_Add"]					= ">> Erdbuff <<";

	L["TerrastraCastBeben"]						= ">> Hol dir den Erd Buff <<";
	L["ArionCastDonnerschock"]					= ">> Hol dir den Luft Buff <<";

	-- Feludius
	L["Feludius_Waterlogged"]					= "Wasserdurchtr\195\164nkt";
	L["Feludius_Waterlogged_Add"]				= ">> Wasserdurchtr\195\164nkt -> Flammenspur <<";
	L["Feludius_Waterlogged_Rem"]				= ">> Wasserdurchtr\195\164nkt weg <<";

	-- Elementium Monstrosity
	L["Elementium_Monstrosity"]					= "Elementiumungeheuer";
	L["Elementium_Monstrosity_Phase3"]			= "SCHMECKT DIE VERDAMMNIS!";

	-- Chogall
	L["Chogall"]								= "Cho'gall";
	L["Chogall_Worshipping"]					= "Verehren";
	L["Chogall_Worshipping_Add"]				= ">> Verehren auf dir <<";
	L["Chogall_Worshipping_Yell_Add"]			= ">> Verehren auf mir ! <<";
	L["Chogall_Worshipping_Yell_Rem"]			= "<< Verehren entfernt >>";
	L["Chogall_Corruption_Sickness"]			= "Verderbnis: Krankheit";
	L["Chogall_Corruption_Sickness_Add"]		= ">> Krankheit auf dir <<";
	L["Chogall_Corruption_Sickness_Yell_Add"]	= ">> Krankheit auf mir ! <<";
	L["Chogall_Creates_Add"]					= "Verderbenden Kultisten beschw\195\182ren";
	L["Chogall_Corrupting_Adherent"]			= "Verderbender Kultist";
	L["Chogall_Phase2"]							= "Bruder, es kann nicht sein... (Das Schicksal ist hier!) Meine St\195\164rke... (Zu stolz! Er spricht durch Blut! Lauscht!) Mein Geist... (Seinem Ruhme dienen wir!) Was... geschieht hier... (Fern ist das Morgenrot. Schatten des Zwielichts, kommt. Wahnsinn, Chaos, unendliche Nacht, kommt, kommt!)";

	-- Anshal
	L["Anshal"]									= "Anshal";
	L["Anshal_Pull"]							= "Ich werde es sein, dem das Wohlwollen unseres Herrn zuf\195\164llt, wenn ich die Eindringlinge bezwinge. Mein sanftester Wind wird sich bereits als ausreichend erweisen!";

	-- Nezir
	L["Nezir"]									= "Nezir";
	L["Nezir_Pull"]								= "Die Ehre, die St\195\182renfriede zu erschlagen, ist die meine, Br\195\188der! Ihre schw\195\164chlichen K\195\182rper werden von meinen eisigen Winden schockgefroren!";

	-- Rohash
	L["Rohash"]									= "Rohash";
	L["Rohash_Ultimate"]						= "Die Macht unserer Winde, ENTFESSELT!";

	-- Asaad
	L["Asaad"]									= "Asaad";
	L["Asaad_Static_Cling"]						= "Statische Aufladung";
	L["Asaad_Static_Cling_Msg"]					= ">> Statische Aufladung, springen <<";

	-- Al'Akir
	L["AlAkir"]									= "Al'Akir";
	L["AlAkir_Pull"]							= "Eure Herausforderung ist angenommen, Sterbliche! Wisset, dass ihr Al'Akir gegen\195\188bersteht, Elementarlord der Luft! Ihr habt keine Aussicht auf Erfolg!";
	L["AlAkir_Phase2"]							= "Eure sinnlose Gegenwehr erz\195\188rnt mich!";
	L["AlAkir_WindBurst"]						= "Windsto\195\159";
	L["AlAkir_WindBrust_Soon_Msg"]				= ">> Windsto\195\159 bald <<";
	L["AlAkir_WindBrust_Msg"]					= ">> Windsto\195\159 <<";

	-- Omnotron
	L["Omnotron_Fixate"]						= "Fixieren";
	L["Omnotron_Fixate_Add"]					= ">> Fixieren <<";
	L["Omnotron_Fixate_Rem"]					= ">> Fixieren weg <<";
	L["Omnotron_Acquiring_Target"]				= "Zielerfassung";
	L["Omnotron_Acquiring_Target_Add"]			= ">> Zielerfassung <<";
	L["Omnotron_Acquiring_Target_Rem"]			= ">> Zielerfassung weg <<";
	L["Omnotron_Acquiring_Target_Yell_Add"]		= ">> Zielerfassung auf mir ! <<";
	L["Omnotron_Acquiring_Target_Yell_Rem"]		= "<< Zielerfassung entfernt >>";
	L["Omnotron_Lightning_Conductor"]			= "Blitzableiter";
	L["Omnotron_Lightning_Conductor_Add"]		= ">> Blitzableiter <<";
	L["Omnotron_Lightning_Conductor_Rem"]		= ">> Blitzableiter weg <<";

	-- Corla
	L["Corla_Evolution"]						= "Evolution";
	L["Corla_Evolution_Rem"]					= ">> Evolution weg <<";

	-- Lord Rhyolith
	L["Lord_Rhyolith"]							= "Lord Rhyolith";
	L["Lord_Rhyolith_Phase2"]					= "\195\132onen habe ich ungest\195\182rt durchgeschlafen\226\128\166 jetzt das\226\128\166 Fleischlinge, Ihr werdet BRENNEN!";
	L["Lord_Rhyolith_Crater"]					= "Krater";
	L["Lord_Rhyolith_Magmaflow"]				= "Magmafluss";
	L["Lord_Rhyolith_Magmaflow_Msg"]			= ">> Magmafluss <<";

	-- Baleroc
	L["Baleroc"]								= "Baloroc";
	L["Baleroc_Torment"] 						= "Qual";
	L["Baleroc_Torment_Msg"] 					= ">> Viele Qualstacks, wechseln <<";
	L["Baleroc_Decimation_Blade"]				= "Dezimierende Klinge";
	L["Baleroc_Decimation_Blade_Msg"]			= ">> Dezimierende Klinge <<";
	L["Baleroc_Inferno_Blade"]					= "Infernoklinge";
	L["Baleroc_Inferno_Blade_Msg"]				= ">> Infernoklinge <<";

	-- Majordomo Staghelm
	L["Staghelm_Searing_Seeds"] 				= "Sengende Samen";
	L["Staghelm_Searing_Seeds_Msg"]				= ">> Sengende Samen <<";

	-- Ragnaros
	L["Ragnaros"]								= "Ragnaros";
	L["Ragnaros_Sulfuras_Smash"]				= "Sulfurasschmettern"
	L["Ragnaros_Sulfuras_Smash_Msg"]			= ">> Sulfurasschmettern <<";

	-- Shannox
	L["Shannox_Rageface"]						= "Augenkratzer";
	L["Shannox_Wary"]							= "Aufmerksam";
	L["Shannox_Feeding_Frenzy"]					= "Fressattacke";
	L["Shannox_Crystal_Prison_Trap"]			= "Kristallgef\195\164ngnisfalle";
	L["Shannox_Crystal_Prison_Trap_Effect"]		= "Kristallgef\195\164ngnisfalleneffekt";
	L["Shannox_Rageface_in_trap_Msg"]		    = ">> Augenkratzer in Falle <<";
	L["Shannox_Rageface_broke_out_Msg"]		    = ">> Augenkratzer ausgebrochen <<";
	L["Shannox_Feeding_Frenzy_expired_Msg"]		= ">> Augenkratzer Fressattacke ausgelaufen <<";
	L["Shannox_Wary_Msg"]						= ">> Augenkratzer Falle m\195\182glich <<";
	L["Shannox_Trap_Immune"]				    = ">> Augenkratzer Fallenimmum f\195\188r 25 seks <<";
	L["Shannox_Feeding_10"]						= ">> Fressattacke Stack hoch <<";
	L["Shannox_Feeding_20"]						= ">> Fressattacke Stack sehr hoch <<";
	L["Shannox_Feeding_30"]						= ">> Fressattacke ist nun t\195\182dlich <<";
	L["Shannox"]								= "Shannox";
	L["Shannox_Pull_Yell"]						= "Ahahahahhah! Die Eindringlinge! T\195\182te sie. FRISS SIE!";

	-- Ultraxion
	L["Ultraxion_Fading_Light"]					= "Schwindendes Licht";
	L["Ultraxion_Fading_Light_Msg"]				= ">> Schwindendes Licht <<";

	-- Zon'ozz
	L["Zonozz_Disrupting_Shadows"]				= "St\195\182rende Schatten";
	L["Zonozz_Disrupting_Shadows_Msg"]			= ">> St\195\182rende Schatten <<";
	L["Zonozz_Disrupting_Shadows_Rem"]			= ">> St\195\182rende Schatten weg <<";

	-- The Stone Guard
	L["Stone_Guard_Amethyst_Guardian"]			= "Amethystw\195\164chter";
	L["Stone_Guard_Amethyst_Pool"]				= "Amethystteich";
	L["Stone_Guard_Amethyst_Pool_Msg"]			= ">> Amethystteich <<";
	L["Stone_Guard_Cobalt_Mine"]				= "Kobaltmine";
	L["Stone_Guard_Cobalt_Mine_Blast"]			= "Kobaltminenexplosion";
	L["Stone_Guard_Cobalt_Mine_Msg"]			= ">> Kobaltmine <<";

	-- Feng
	L["Feng_Wildfire_Spark"]					= "Wildfeuerfunke";
	L["Feng_Wildfire_Spark_Msg"]				= ">> Wildfeuerfunke <<"
	L["Feng_Wildfire_Spark_Rem"]				= ">> Wildfeuerfunke weg <<";

	-- Gara'jal the Spiritbinder
	L["Garajal_Voodoo_Doll"]					= "Voodoopuppe";
	L["Garajal_Voodoo_Doll_Msg"]				= ">> Voodoopuppe <<"
	L["Garajal_Voodoo_Doll_Rem"]				= ">> Voodoopuppe weg <<";
	L["Garajal_Troll_Rush"]						= "Trollansturm";
	L["Garajal_Troll_Rush_Msg"]					= ">> Trollansturm <<";
	L["Garajal_Troll_Rush_Rem"]					= ">> Trollansturm weg <<";
	L["Garajal_Revitalized_Spirit"]				= "Revitalisierter Geist";
	L["Garajal_Revitalized_Spirit_Msg"] 		= ">> Du kannst nun die Geisterwelt verlassen <<";
	L["Garajal_Revitalized_Spirit_Yell"]		= ">> Ich kann nun die Geisterwelt verlassen <<";

	-- Vizier Zor'lok
	L["Vizier_Zorlok"]							= "Kaiserlicher Wesir Zor'lok";
	L["Vizier_Zorlok_Noise_Cancelling"]			= "Schallschutz";
	L["Vizier_Zorlok_Force_and_Verve"]			= "Kraft und Schwung";
	L["Vizier_Zorlok_Attenuation"]				= "Abschw\195\164chung";
	L["Vizier_Zorlok_Attenuation_Msg"]			= ">> Abschw\195\164chung <<";
	L["Vizier_Zorlok_Pull"]						= "Die G\195\182ttliche erw\195\164hlte uns als sterbliche Stimme f\195\188r Ihren g\195\182ttlichen Willen. Wir existieren, um Ihren Willen auszuf\195\188hren.";

	-- Imperator Mar'gok
	L["Imperator_Margok"]										= "Kaiser Mar'gok";
	L["Imperator_Margok_Force_Nova"]							= "Machtnova";
	L["Imperator_Margok_Force_Nova_Displacement"]				= "Machtnova: Verschiebung";
	L["Imperator_Margok_Force_Nova_Fortification"]				= "Machtnova: St\195\164rkung";
	L["Imperator_Margok_Force_Nova_Replication"]				= "Machtnova: Reproduktion";
	L["Imperator_Margok_Force_Nova_Msg"]						= ">> Machtnova, spring dr\195\188ber <<";

	L["Imperator_Margok_Destructive_Resonance"] 				= "Destruktive Resonanz";
	L["Imperator_Margok_Destructive_Resonance_Displacement"]	= "Destruktive Resonanz: Verschiebung";
	L["Imperator_Margok_Destructive_Resonance_Fortification"]	= "Destruktive Resonanz: St\195\164rkung";
	L["Imperator_Margok_Destructive_Resonance_Replication"]		= "Destruktive Resonanz: Reproduktion";
	L["Imperator_Margok_Destructive_Resonance_Msg"] 			= ">> Destruktive Resonanz <<";

	L["Imperator_Margok_Branded"]								= "Gebrandmarkt";
	L["Imperator_Margok_Branded_Displacement"]					= "Gebrandmarkt: Verschiebung";
	L["Imperator_Margok_Branded_Fortification"]					= "Gebrandmarkt: St\195\164rkung";
	L["Imperator_Margok_Branded_Replication"]					= "Gebrandmarkt: Reproduktion";
	L["Imperator_Margok_Branded_Msg"]							= ">> Gebrandmarkt <<";
	L["Imperator_Margok_Branded_Rem"]							= ">> Gebrandmarkt weg <<";

	-- Blast Furnace
	L["Blast_Furnace"]											= "Schmelzofen";
	L["Blast_Furnace_Bellows_Operator"]							= "Gebl\195\164searbeiter";
	L["Blast_Furnace_Heat_Regulator"]							= "Hitzeregler";
	L["Blast_Furnace_Loading"]									= "Lade";
	L["Blast_Furnace_Primal_Elementalist"]						= "Urelementaristin";
	L["Blast_Furnace_Primal_Elementalist_Schilds_Down"]			= "Schilde unten";
	L["Blast_Furnace_Primal_Elementalist_Schilds_Down_Msg"]		= ">> Schilde unten <<";
	L["Blast_Furnace_Primal_Elementalist_Schilds_Down_Rem"]		= ">> Schilde oben <<";

	-- Gorefiend
	L["Gorefiend"]												= "Blutschatten";
	L["Gorefiend_Digest"]										= "Verdauen";
	L["Gorefiend_Digest_Add"]									= ">> Verdauen <<";
	L["Gorefiend_Digest_Rem"]									= ">> Verdauen weg <<";
	L["Gorefiend_Shared_Fate"]									= "Geteiltes Schicksal";
	L["Gorefiend_Shared_Add"]									= ">> Geteiltes Schicksal <<";
	L["Gorefiend_Shared_Rem"]									= ">> Geteiltes Schicksal weg <<";
	L["Gorefiend_Shared_RunToMe"]								= "<< Zu mir laufen >>";
	L["Gorefiend_Shared_HaveToRun"]								= "> Ich muss laufen <";
	L["Gorefiend_Touch_of_Doom"]								= "Ber\195\188hrung der Verdammnis";
	L["Gorefiend_Touch_of_Doom_Add"]							= ">> Ber\195\188hrung der Verdammnis <<";
	L["Gorefiend_Touch_of_Doom_Rem"]							= ">> Ber\195\188hrung der Verdammnis weg <<";

	-- Tyrant Velhari
	L["TyrantVelhari"]											= "Tyrannin Velhari";
	L["TyrantVelhari_Font_of_Corruption"]						= "Quell der Verderbnis";
	L["TyrantVelhari_Font_of_Corruption_Add"]					= ">> Quell der Verderbnis <<";
	L["TyrantVelhari_Font_of_Corruption_Rem"]					= ">> Quell der Verderbnis weg <<";
	L["TyrantVelhari_Aura_of_Contempt"]							= "Aura der Verachtung";
	L["TyrantVelhari_Aura_of_Contempt_Msg"]						= ">> Aura der Verachtung <<";

	-- Xhul'horac
	L["Xhulhorac_Fel_Surge"]									= "Teufelssog"
	L["Xhulhorac_Fel_Surge_Add"]								= ">> Teufelssog <<"
	L["Xhulhorac_Fel_Surge_Rem"]								= ">> Teufelssog weg <<"
	L["Xhulhorac_Void_Surge"]									= "Leerensog"
	L["Xhulhorac_Void_Surge_Add"]								= ">> Leerensog <<"
	L["Xhulhorac_Void_Surge_Rem"]								= ">> Leerensog weg <<"

	-- Kilrogg Deadeye
	L["Kilrogg_Deadeye_Salivating_Bloodthirster"]				= "Geifernder Blutd\195\188rster";
	L["Kilrogg_Deadeye_Fel_Claws"]								= "Teufelskrallen";

	-- Socrethar
	L["Socrethar_Gift_Manari"]									= "Gabe der Man'ari";
	L["Socrethar_Gift_Manari_Add"]								= ">> Gabe der Man'ari <<";
	L["Socrethar_Gift_Manari_Rem"]								= ">> Gabe der Man'ari weg <<";

	-- Patrol Captain Gerdo
	L["CaptainGerdo_Arcane_Lockdown"]							= "Arkane Abriegelung";
	L["CaptainGerdo_Arcane_Lockdown_Add"]						= ">> Arkane Abriegelung: springen <<";
	L["CaptainGerdo_Arcane_Lockdown_Rem"]						= ">> Arkane Abriegelung weg <<";

	-- Blood-Princess Thal'ena
	L["Thalena_Essence"]										= "Essenz der Blutprinzessin";

	L["Thalena_Frenzied_Bloodthirst"]							= "Rasender Blutdurst";
	L["Thalena_Frenzied_Bloodthirst_Add"]						= ">> Rasender Blutdurst: Bei\195\159en jetzt! <<";
	L["Thalena_Frenzied_Bloodthirst_Rem"]						= ">> Rasender Blutdurst weg <<";

end
