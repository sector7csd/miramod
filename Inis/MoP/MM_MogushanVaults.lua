function MiraMod_MogushanVaults_Init(ini)

	-- Debuffs
	-- ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)

	-- Wildfeuerfunke (Feng der Verfluchte)
	ini:addDebuff("Feng_Wildfire_Spark", "seuche", "Feng_Wildfire_Spark_Msg", "verbindungweg", "Feng_Wildfire_Spark_Rem", false, nil, nil, 0, true);

	-- Voodoo Puppe (Gara'jal der Geisterbinder)
	ini:addDebuff("Garajal_Voodoo_Doll", "seuche", "Garajal_Voodoo_Doll_Msg", "verbindungweg", "Garajal_Voodoo_Doll_Rem");

	-- Trollansturm (Gara'jal der Geisterbinder, Trash)
	ini:addDebuff("Garajal_Troll_Rush", "lauf", "Garajal_Troll_Rush_Msg", "verbindungweg", "Garajal_Troll_Rush_Rem");

	-- Revitalisierter Geist (Gara'jal der Geisterbinder)
	ini:addDebuff("Garajal_Revitalized_Spirit", "seuche", "Garajal_Revitalized_Spirit_Msg", nil, nil, false, "Garajal_Revitalized_Spirit_Yell");

	-- Combat
	-- ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)
end
