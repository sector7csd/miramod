function MiraMod_SiegeofOrgrimmar_Init(ini)

	ini.onStartEncounter = MiraMod_SiegeofOrgrimmar_Encounter_Start;
	ini.onYell			 = MiraMod_SiegeofOrgrimmar_OnYell;

	local playerName = GetUnitName("player", false);

	-- function ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)
	ini:addCombat("ShaofPride", 				   	"SPELL_CAST_SUCCESS", 	144400, "ShaofPride_SwellingPride", 		nil, 		nil, 							 		MiraMod_OnShaCastsSwellingPride);
	ini:addCombat("Fallen_Protectors_He_Softfoot", 	"SPELL_CAST_START",		143330, "Fallen_Protectors_Gouge",  		"gouge", 	"Fallen_Protectors_Gouge_Msg");
	ini:addCombat("IronJuggernaut_CutterLaser", 	"SPELL_CAST_SUCCESS", 	146325, "IronJuggernaut_CutterLaserTarget", "lauf", 	"IronJuggernaut_CutterLaserTarget_Msg", nil, 0, playerName);
	ini:addCombat("IronJuggernaut",					"SPELL_CAST_START",	  	144483, "IronJuggernaut_SeismicActivity",   nil,		nil,									MiraMod_OnIronJuggernautCastsSeismicActivity);
	ini:addCombat("IronJuggernaut",					"SPELL_AURA_REMOVED",	144483, "IronJuggernaut_SeismicActivity",   nil,		nil,									MiraMod_OnIronJuggernautDoneSeismicActivity);

	-- function ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)
	ini:addDebuff("ShaofPride_GiftoftheTitans",  "gift",       "ShaofPride_GiftoftheTitans_Add",  "giftexpired",   "ShaofPride_GiftoftheTitans_Rem");
	ini:addDebuff("ShaofPride_PoweroftheTitans", "power",      "ShaofPride_PoweroftheTitans_Add", "powerexpired",  "ShaofPride_PoweroftheTitans_Rem");
	ini:addDebuff("ShaofPride_WeakenedResolve",  "riftdebuff", "ShaofPride_WeakenedResolve_Add",  "verbindungweg", "ShaofPride_WeakenedResolve_Rem", false, nil, "ShaofPride_WeakenedResolve_Yell_Rem", nil, true);
	ini:addDebuff("ShaofPride_Projection", 		 "hinlauf",	   "ShaofPride_Projection_Add",		  nil, 				"ShaofPride_Projection_Rem");
	ini:addDebuff("ShaofPride_Banishment",		 "newadd",	   "ShaofPride_Banishment_Add",		  "adddown",	   "ShaofPride_Banishment_Rem", true);

end

function MiraMod_SiegeofOrgrimmar_Encounter_Start(encounterId, encounterName, diff, players)

	-- MiraMod_PlaySound("deciblade");

	if(encounterName == MiraMod_GetTranslation("ShaofPride")) then

		MiraMod_OnShaCastsSwellingPride();

	end

	if(encounterName == MiraMod_GetTranslation("IronJuggernaut")) then

		MiraMod_OnIronJuggernautDoneSeismicActivity();

	end

end

function MiraMod_OnShaCastsSwellingPride()

	MiraMod_Write("OnShaCastsSwellingPride");

	-- Sha casts Swelling Pride every 77 secs, warn 10 secs before
	-- MiraMod_AddSpecialTimer(EndTime, Msg, Snd, Function, Name)
	MiraMod_AddSpecialTimer(time() + 67, MiraMod_GetTranslation("ShaofPride_SwellingPride_Soon"), "swellingpridesoon", nil, "SwellingPride");

end

function MiraMod_OnIronJuggernautCastsSeismicActivity()

	MiraMod_Write("OnIronJuggernautCastsSeismicActivity");

	local now = time();

	-- When Iron Juggernaut casts SeismicActivity then we are in siege mode for 62 secs (2 to cast) - warn 10 secs before for next phase
	-- MiraMod_AddSpecialTimer(EndTime, Msg, Snd, Function, Name)
	MiraMod_AddSpecialTimer(now + 52, MiraMod_GetTranslation("NextPhaseSoon"), "nextphasesoon", nil);

	-- This also triggers the timers for shock pulse: 18 secs, 35 secs, 51 secs - warn each 3 secs before
	MiraMod_AddSpecialTimer(now + 15, MiraMod_GetTranslation("IronJuggernaut_ShockPulse_Msg"), "seuche", nil);
	MiraMod_AddSpecialTimer(now + 32, MiraMod_GetTranslation("IronJuggernaut_ShockPulse_Msg"), "seuche", nil);
	MiraMod_AddSpecialTimer(now + 48, MiraMod_GetTranslation("IronJuggernaut_ShockPulse_Msg"), "seuche", nil);

end

function MiraMod_OnIronJuggernautDoneSeismicActivity()

	MiraMod_Write("OnIronJuggernautDoneSeismicActivity");

	-- When Iron Juggernaut removes his SeismicActivity buff then we've 120 secs until siege mode - warn 10 secs before for next phase
	-- MiraMod_AddSpecialTimer(EndTime, Msg, Snd, Function)
	MiraMod_AddSpecialTimer(time() + 110, MiraMod_GetTranslation("NextPhaseSoon"), "nextphasesoon", nil);

end

function MiraMod_SiegeofOrgrimmar_OnYell(what, who)

	if(who == MiraMod_GetTranslation("Norushen")) then

		if(what == MiraMod_GetTranslation("Norushen_StopIt")) then

			-- reset Swelling Pride timer
			MiraMod_CancelSpecialTimer("SwellingPride");
			MiraMod_OnShaCastsSwellingPride();

		end

	end

end
