function MiraMod_HeartofFear_Init(ini)

	-- Imperial Vizier Zor'lok Stuff
	NoiseCancellingActive			= false;

	-- Debuffs
	-- ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)

	-- Combat
	-- ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)

	-- Vizier Zor'lok, Noise Cancelling
	ini:addCombat("", "SPELL_CAST_SUCCESS", 122706, "Vizier_Zorlok_Noise_Cancelling", nil, nil, MiraMod_OnZorlokNoiseCanellingDetected);

	-- Vizier Zor'lok, Force and Verve aura removed
	ini:addCombat("Vizier_Zorlok", "SPELL_AURA_REMOVED", 122713, "Vizier_Zorlok_Force_and_Verve", nil, nil, MiraMod_OnZorlokForceAndVerceDone);

	-- Vizier Zor'lok, Attenuation
	ini:addCombat("Vizier_Zorlok", "SPELL_CAST_START", 122474, "", "Vizier_Zorlok_Attenuation_Msg", "seuche");
	--                                                          ^- Empty because, we casts Attenuation twice, but with different spell id's, so trigger by name would cause double annoucement

	-- Yells
	ini.onYell = MiraMod_HeartofFear_Yell;

end

function MiraMod_HeartofFear_Yell(arg1, arg2)

	if(arg2 == MiraMod_GetTranslation("Vizier_Zorlok")) then

		-- Pull
		if(arg1 == MiraMod_GetTranslation("Vizier_Zorlok_Pull")) then

		NoiseCancellingActive = false;

		end

	end

end

function MiraMod_OnZorlokNoiseCanellingDetected()

	if (NoiseCancellingActive == false) then

		NoiseCancellingActive = true;

		MiraModTextDisplay:AddMessage(">> 1 <<", 1.0, 1.0, 0.55);
		MiraMod_PlaySound("1");

		MiraMod_AddSpecialTimer(time() + 1, ">> 2 <<", "2", nil);
		MiraMod_AddSpecialTimer(time() + 2, ">> 3 <<", "3", nil);

	end

end

function MiraMod_OnZorlokForceAndVerceDone()

	-- 5 secs after Zorlok has finished casting Force and Verce, reset the flag for NoiseCancelling
	MiraMod_AddSpecialTimer(time() + 5, nil, nil, MiraMod_OnZorlokForceAndVerceResetFlag);

end

function MiraMod_OnZorlokForceAndVerceResetFlag()

	NoiseCancellingActive = false;

end
