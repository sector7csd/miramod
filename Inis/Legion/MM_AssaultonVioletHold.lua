function MiraMod_AssaultonVioletHold_Init(ini)

	-- Debuffs
	-- ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)

	-- Essence of the Blood Princess, Blood-Princess Thal'ena
	ini:addDebuff("Thalena_Essence", nil, nil, nil, nil, false, nil, nil, nil, true);

	-- Frenzied Bloodthirst, Blood-Princess Thal'ena
	ini:addDebuff("Thalena_Frenzied_Bloodthirst", "seuche", "Thalena_Frenzied_Bloodthirst_Add", "verbindungweg", "Thalena_Frenzied_Bloodthirst_Rem", false, nil, nil, nil, true);

	-- Combat
	-- ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)

end
