function MiraMod_BlackwingDescent_Init(ini)

	-- Debuffs
	-- ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)

	-- Zielerfassung (Omnotronverteidigungssystem, Pechschwingenabstieg)
	ini:addDebuff("Omnotron_Acquiring_Target", "lauf", "Omnotron_Acquiring_Target_Add", "verbindungweg", "Omnotron_Acquiring_Target_Rem", false, "Omnotron_Acquiring_Target_Yell_Add", "Omnotron_Acquiring_Target_Yell_Rem");

	-- Todesurteil (Atramedes-Trash, Pechschwingenabstieg)
	ini:addDebuff("Atramedes_Execution_Sentence", "seuche", "Atramedes_Execution_Sentence_Add", "verbindungweg", "Atramedes_Execution_Sentence_Rem", false, "Atramedes_Execution_Sentence_Yell_Add", "Atramedes_Execution_Sentence_Yell_Rem");

	-- Verzehrende Flammen (Maloriak, Pechschwingenabstieg)
	ini:addDebuff("Maloriak_Flames", "lauf", "Maloriak_Flames_Add", "verbindungweg", "Maloriak_Flames_Rem");

	-- Blitzeis (Maloriak, Pechschwingenabstieg)
	ini:addDebuff("Maloriak_Freeze", "seuche", "Maloriak_Freeze_Add", "verbindungweg", "Maloriak_Freeze_Rem", false, "Maloriak_Freeze_Yell_Add", "Maloriak_Freeze_Yell_Rem");

	-- Entkräftender Schleim (Maloriak, Pechschwingenabstieg)
	ini:addDebuff("Maloriak_Slime", "seuche", "Maloriak_Slime_Add", "verbindungweg", "Maloriak_Slime_Rem", false, nil, nil, nil, false, 13);

	-- Fixieren (Omnotronverteidigungssystem, Pechschwingenabstieg)
	ini:addDebuff("Omnotron_Fixate", "lauf", "Omnotron_Fixate_Add", "verbindungweg", "Omnotron_Fixate_Rem");

	-- Blitzableiter (Omnotronverteidigungssystem, Pechschwingenabstieg)
	ini:addDebuff("Omnotron_Lightning_Conductor", "lauf", "Omnotron_Lightning_Conductor_Add", "verbindungweg", "Omnotron_Lightning_Conductor_Rem");

	-- Combat
	-- ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)

	-- Arkaner Sturm (Maloriak, Pechschwingenabstieg)
	ini:addCombat("Maloriak", "SPELL_CAST_SUCCESS", 0, "Maloriak_Storm", "unterbrechen", "Maloriak_Storm_Msg");

	-- Fehde (Schimaeron, Pechschwingenabstieg)
	ini:addCombat("Chimaeron", "SPELL_CAST_SUCCESS", 88872, "Chimaeron_Feud", "hinlauf", "Chimaeron_Feud_Msg");

	-- Doppelangriff (Schimaeron, Pechschwingenabstieg)
	ini:addCombat("Chimaeron", "SPELL_AURA_APPLIED", 88826, "Chimaeron_DoubleAttack", "doppelangriff", "Chimaeron_DoubleAttack_Msg");

	-- Sterblichkeit (Schimaeron, Pechschwingenabstieg)
	ini:addCombat("Chimaeron", "SPELL_CAST_SUCCESS", 82890, "Chimaeron_Mortality", "phase2", "Phase2");

	-- Sengende Flamme (Atramedes, Pechschwingenabstieg)
	ini:addCombat("Atramedes", "SPELL_CAST_SUCCESS", 77840, "Atramedes_Flame", "unterbrechen", "Atramedes_Flame_Msg");

	-- Phase Shift Removal (Atramedes, Pechschwingenabstieg)
	ini:addCombat("Atramedes_Obnoxious_Fiend", "SPELL_AURA_REMOVED", 92681, "Atramedes_Obnoxious_Fiend_PhaseShift", "newadd", "NewAdd");

	-- Obnoxious Fiend dies (Atramedes, Pechschwingenabstieg)
	ini:addCombat("Atramedes_Obnoxious_Fiend", "UNIT_DIED", 0, nil, "adddown", "AddDown");

	-- Yells
	ini.onYell = MiraMod_BlackwingDescent_Yell;

end

function MiraMod_BlackwingDescent_Yell(arg1, arg2)

	if(arg2 == MiraMod_GetTranslation("Atramedes")) then

		-- Flugphase
		if(arg1 == MiraMod_GetTranslation("Atramedes_AirPhase")) then
			MiraMod_PlaySound("hebtab");
			MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("AirPhase"), 1.0, 0.32, 0.66);

			-- Add landing warning here, ground phase will start after 30 secs
			MiraMod_AddSpecialTimer(time() + 25, MiraMod_GetTranslation("NextPhaseSoon"), "nextphasesoon", nil);

			-- Add flame warning here, flame will start after 78 secs
			MiraMod_AddSpecialTimer(time() + 73, MiraMod_GetTranslation("Atramedes_Flame_Soon_Msg"), "flammebald", nil);

			-- Add flyphase warning here, will start after 120 secs
			MiraMod_AddSpecialTimer(time() + 115, MiraMod_GetTranslation("AirPhase_Soon"), "flugphasebald", nil);
		end

		-- Pull
		if(arg1 == MiraMod_GetTranslation("Atramedes_Pull")) then

			-- Add flame warning here, flame will start after 45 secs
			MiraMod_AddSpecialTimer(time() + 40, MiraMod_GetTranslation("Atramedes_Flame_Soon_Msg"), "flammebald", nil);

			-- Add flyphase warning here, will start after 95 secs
			MiraMod_AddSpecialTimer(time() + 90, MiraMod_GetTranslation("AirPhase_Soon"), "flugphasebald", nil);

		end

	end

	if(arg2 == MiraMod_GetTranslation("Maloriak")) then

		-- Phase 2
		if(arg1 == MiraMod_GetTranslation("Maloriak_Low_HP_1") or arg1 == MiraMod_GetTranslation("Maloriak_Low_HP_2")) then
			MiraMod_PlaySound("phase2");
			MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Phase2"), 1.0, 0.32, 0.66);
		end

		-- Blaue Phase
		if(arg1 == MiraMod_GetTranslation("Maloriak_Blue_Phase")) then
			MiraMod_PlaySound("blauephase");
			MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Maloriak_Blue_Phase_Msg"), 0.32, 0.32, 1.00);
			MiraMod_AddSpecialTimer(time() + 40, MiraMod_GetTranslation("NextPhaseSoon"), "nextphasesoon", nil);
		end

		-- Rote Phase
		if(arg1 == MiraMod_GetTranslation("Maloriak_Red_Phase")) then
			MiraMod_PlaySound("rotephase");
			MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Maloriak_Red_Phase_Msg"), 1.0, 0.32, 0.32);
			MiraMod_AddSpecialTimer(time() + 40, MiraMod_GetTranslation("NextPhaseSoon"), "nextphasesoon", nil);
		end

		-- Grüne Phase
		if(arg1 == MiraMod_GetTranslation("Maloriak_Green_Phase")) then
			MiraMod_PlaySound("gruenephase");
			MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Maloriak_Green_Phase_Msg"), 0.32, 1.00, 0.32);
			MiraMod_AddSpecialTimer(time() + 40, MiraMod_GetTranslation("NextPhaseSoon"), "nextphasesoon", nil);
		end

	end

end
