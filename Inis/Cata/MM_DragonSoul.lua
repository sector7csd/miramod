function MiraMod_DragonSoul_Init(ini)

	-- Debuffs
	-- ini:AddDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)

	-- Schwindendes Licht (Ultraxion, Drachenseele)
	ini:addDebuff("Ultraxion_Fading_Light", nil, "Ultraxion_Fading_Light_Msg", nil, nil, false, nil, nil, 0, true);

	-- Störende Schatten (Kriegsherr Zon'ozz, Drachenseele)
	ini:addDebuff("Zonozz_Disrupting_Shadows", "seuche", "Zonozz_Disrupting_Shadows_Msg", "verbindungweg", "Zonozz_Disrupting_Shadows_Rem");

	-- Combat
	-- ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)

end
