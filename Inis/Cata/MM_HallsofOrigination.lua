function MiraMod_HallsofOrigination_Init(ini)

	-- Debuffs
	-- ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)

	-- Combat
	-- ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)

    -- Kugelgefängnis (Hallen des Urspungs Trash)
    ini:addDebuff("BubbleBound", "seuche", "BubbleBound_Add", "verbindungweg", "BubbleBound_Rem", false, "BubbleBound_Yell_Add", "BubbleBound_Yell_Rem");

end
