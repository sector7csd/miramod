function MiraMod_Firelands_Init(ini)

	-- Shannox Stuff
	RagefaceInTrap					= false;

	-- Debuffs
	-- ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)

	-- Qual (Baloroc, Feuerlande)
	ini:addDebuff("Baleroc_Torment", "lauf", "Baleroc_Torment_Msg", nil, nil, false, nil, nil, 12);

	-- Sengende Samen (Majordomus Hirschhaupt, Feuerlande)
	ini:addDebuff("Staghelm_Searing_Seeds", nil, "Staghelm_Searing_Seeds_Msg", nil, nil, false, nil, nil, 0, true);

	-- Combat
	-- ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)

	-- Eruption (Lord Rhyolith)
	ini:addCombat("Lord_Rhyolith_Crater", "SPELL_CAST_SUCCESS", 97225, "Lord_Rhyolith_Magmaflow", "seuche", "Lord_Rhyolith_Magmaflow_Msg");

	-- Decimation Blade (Baloroc, Feuerlande)
	ini:addCombat("Baleroc", "SPELL_CAST_START", 99352, "Baleroc_Decimation_Blade", "deciblade", "Baleroc_Decimation_Blade_Msg");

	-- Inferno Blade (Baloroc, Feuerlande)
	ini:addCombat("Baleroc", "SPELL_CAST_START", 99350, "Baleroc_Inferno_Blade", "infeblade", "Baleroc_Inferno_Blade_Msg");

	-- Sulfuras Smash (Ragnaros, Feuerlande)
	ini:addCombat("Ragnaros", "SPELL_CAST_START", 98710, "Ragnaros_Sulfuras_Smash", "seuche", "Ragnaros_Sulfuras_Smash_Msg");

	-- Augenkratzer in Falle (Shannox, Feuerlande)
	ini:addCombat("Shannox_Crystal_Prison_Trap", "SPELL_AURA_APPLIED", 99837, "Shannox_Crystal_Prison_Trap_Effect", "ragefaceintrap", "Shannox_Rageface_in_trap_Msg", MiraMod_OnRagefaceEntersTrap, nil, "Shannox_Rageface");

	-- Augenkratzer aus Falle ausgebrochen (Shannox, Feuerlande)
	ini:addCombat("Shannox_Crystal_Prison_Trap", "SPELL_AURA_REMOVED", 99837, "Shannox_Crystal_Prison_Trap_Effect", "ragefacebrokeout", "Shannox_Rageface_broke_out_Msg", MiraMod_OnRagefaceLeavesTrap, nil, "Shannox_Rageface");

	-- Augenkratzer Fressattacke ausgelaufen (Shannox, Feuerlande)
	ini:addCombat("Shannox_Rageface", "SPELL_AURA_REMOVED", 100656, "Shannox_Feeding_Frenzy", "feedingexpired", "Shannox_Feeding_Frenzy_expired_Msg");

	-- Augenkratzer Aufmerksam ausgelaufen (Shannox, Feuerlande)
	ini:addCombat("Shannox_Rageface", "SPELL_AURA_REMOVED", 101216, "Shannox_Wary", "trappossible", "Shannox_Wary_Msg");

	-- Augenkratzer, Aufmerksam bekommen (Shannox, Feuerlande)
	ini:addCombat("Shannox_Rageface", "SPELL_AURA_APPLIED", 101216, "Shannox_Wary", nil, nil, MiraMod_OnRagefaceGetsWary);

	-- Augenkratzer, Fressattacke 10 Stacks (Shannox, Feuerlande)
	ini:addCombat("Shannox_Rageface", "SPELL_AURA_APPLIED_DOSE", 100656, "Shannox_Feeding_Frenzy", "feeding10", "Shannox_Feeding_10", nil, 10);

	-- Augenkratzer, Fressattacke 20 Stacks (Shannox, Feuerlande)
	ini:addCombat("Shannox_Rageface", "SPELL_AURA_APPLIED_DOSE", 100656, "Shannox_Feeding_Frenzy", "feeding20", "Shannox_Feeding_20", nil, 20);

	-- Augenkratzer, Fressattacke 30 Stacks (Shannox, Feuerlande)
	ini:addCombat("Shannox_Rageface", "SPELL_AURA_APPLIED_DOSE", 100656, "Shannox_Feeding_Frenzy", "feeding30", "Shannox_Feeding_30", nil, 30);

	-- Yells
	ini.onYell = MiraMod_Firelands_Yell;

end

function MiraMod_Firelands_Yell(arg1, arg2)

	if(arg2 == MiraMod_GetTranslation("Lord_Rhyolith")) then

		-- Phase 2
		if(arg1 == MiraMod_GetTranslation("Lord_Rhyolith_Phase2")) then
			MiraMod_PlaySound("phase2");
			MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Phase2"), 1.0, 0.32, 0.66);

		end

	end

	if(arg2 == MiraMod_GetTranslation("Shannox")) then

		-- Pull
		if(arg1 == MiraMod_GetTranslation("Shannox_Pull_Yell")) then

			RagefaceInTrap = false;

		end

	end

end

function MiraMod_OnRagefaceEntersTrap()

	RagefaceInTrap = true;

end

function MiraMod_OnRagefaceLeavesTrap()

	RagefaceInTrap = false;

end

function MiraMod_OnRagefaceGetsWary()

	if(RagefaceInTrap == false) then

		MiraMod_PlaySound("trapimmune");
		MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Shannox_Trap_Immune"), 1.0, 0.32, 0.66);

	end

end
