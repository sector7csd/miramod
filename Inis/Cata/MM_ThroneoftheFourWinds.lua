function MiraMod_FourWinds_Init(ini)

	-- Debuffs
	-- ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)

	-- Combat
	-- ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)
	ini:addCombat("AlAkir", "SPELL_CAST_START", 87770, "AlAkir_WindBurst", "seuche", "AlAkir_WindBrust_Msg", MiraMod_OnAlAkirCastWindBurst);

	ini.onYell = MiraMod_FourWinds_Yell;

end

function MiraMod_FourWinds_Yell(arg1, arg2)

	if(arg2 == MiraMod_GetTranslation("Anshal") or arg2 == MiraMod_GetTranslation("Nezir")) then

		-- Pull
		if(arg1 == MiraMod_GetTranslation("Anshal_Pull") or arg1 == MiraMod_GetTranslation("Nezir_Pull")) then

			MiraMod_AddSpecialTimer(time() + 77, MiraMod_GetTranslation("NextPhaseSoon"), "nextphasesoon", nil);

		end

	end

	if(arg2 == MiraMod_GetTranslation("Rohash")) then

		-- Start von Ultimate
		if(arg1 == MiraMod_GetTranslation("Rohash_Ultimate")) then

			MiraMod_AddSpecialTimer(time() + 97, MiraMod_GetTranslation("NextPhaseSoon"), "nextphasesoon", nil);

		end

	end

	if(arg2 == MiraMod_GetTranslation("AlAkir")) then

		-- Phase 2
		if(arg1 == MiraMod_GetTranslation("AlAkir_Phase2")) then
			MiraMod_PlaySound("phase2");
			MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Phase2"), 1.0, 0.32, 0.66);

		end

		-- Windburst
		if(arg1 == MiraMod_GetTranslation("AlAkir_WindBurst")) then

			MiraMod_AddSpecialTimer(time() + 20, MiraMod_GetTranslation("AlAkir_WindBrust_Soon_Msg"), "windburst", nil);

		end

	end

end

function MiraMod_OnAlAkirCastWindBurst()

	MiraMod_AddSpecialTimer(time() + 20, MiraMod_GetTranslation("AlAkir_WindBrust_Soon_Msg"), "windburst", nil);

end
