function MiraMod_TheBastionofTwilight_Init(ini)

	-- Debuffs
	-- ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)

	-- Umhüllende Magie (Theralion & Valiona, Bastion des Zwielichtes)
	ini:addDebuff("Valiona_Engulfing_Magic", "lauf", "Valiona_Engulfing_Magic_Add", "verbindungweg", "Valiona_Engulfing_Magic_Rem");

	-- Blackout (Theralion & Valiona, Bastion des Zwielichtes)
	ini:addDebuff("Valiona_Blackout", "hinlauf", "Valiona_Blackout_Add", "verbindungweg", "Valiona_Blackout_Rem", true);

	-- Zwielichtmeteorit (Theralion & Valiona, Bastion des Zwielichtes)
	ini:addDebuff("Valiona_Twilight_Meteorite", "hinlauf", "Valiona_Twilight_Meteorite_Add", "verbindungweg", "Valiona_Twilight_Meteorite_Rem");

	-- Wasserdurchtränkt (Rat der Aszendentfürsten, Bastion des Zwielichtes)
	ini:addDebuff("Feludius_Waterlogged", "hinlauf", "Feludius_Waterlogged_Add", "verbindungweg", "Feludius_Waterlogged_Rem");

	-- Blitzableiter (Rat der Aszendentfürsten, Bastion des Zwielichtes)
	ini:addDebuff("Arion_Lightning_Rod", "lauf", "Arion_Lightning_Rod_Add", "verbindungweg", "Arion_Lightning_Rod_Rem");

	-- Verehren (Cho'gall, Bastion des Zwielichtes)
	ini:addDebuff("Chogall_Worshipping", "seuche", "Chogall_Worshipping_Add", "verbindungweg", "Chogall_Worshipping_Yell_Add", "Chogall_Worshipping_Yell_Rem");

	-- Dunkler Teich (Halfus Trash, Bastion des Zwielichtes)
	ini:addDebuff("Halfus_DarkPool", "lauf", "Halfus_DarkPool_Add", "verbindungweg", "Halfus_DarkPool_Rem");

	-- Wirbelnde Winde (Rat der Aszendentfürsten, Bastion des Zwielichts)
	ini:addDebuff("Arion_Winds", "luftbuff", "Arion_Winds_Add");

	-- Geerdet (Rat der Aszendentfürsten, Bastion des Zwielichts)
	ini:addDebuff("Terrastra_Grounded", "erdbuff", "Terrastra_Grounded_Add");

	-- Verderbnis: Krankheit (Cho'gall, Bastion des Zwielichtes)
	ini:addDebuff("Chogall_Corruption_Sickness", "lauf", "Chogall_Corruption_Sickness_Add", nil, nil, "Chogall_Corruption_Sickness_Yell_Add");

	-- Combat
	-- ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)

	-- Verschlingende Flammen (Valiona, Boss 3 Grim Bartol & Boss 2 Bastion des Zwielichts)
	ini:addCombat("Valiona", "SPELL_CAST_START", 0, "Valiona_Flames", "lauf", "Valiona_Flames_Msg");

	-- Beben (Rat der Aszendentfürsten, Bastion des Zwielichts)
	ini:addCombat("Terrastra", "SPELL_CAST_START", 83565, "Terrastra_Quake", nil, nil, MiraMod_OnTerrastraCastBeben);

	-- Donnerschock (Rat der Aszendentfürsten, Bastion des Zwielichts)
	ini:addCombat("Arion", "SPELL_CAST_START", 83067, "Arion_Thundershock", nil, nil, MiraMod_OnArionCastDonnerschock);

	-- Summon Corrupting Adherent (Cho'gall, Bastion des Zwielichts)
	ini:addCombat("Chogall", "SPELL_CAST_START", 81628, "Chogall_Creates_Add", "newadd", "NewAdd");

	-- Corrupting Adherent dies (Cho'gall, Bastion des Zwielichts)
	ini:addCombat("Chogall_Corrupting_Adherent", "UNIT_DIED", 0, nil, "adddown", "AddDown");

	-- Yells
	ini.onYell = MiraMod_TheBastionofTwilight_Yell;

end

function MiraMod_TheBastionofTwilight_Yell(arg1, arg2)

	if(arg2 == MiraMod_GetTranslation("Arion")) then
		-- Phase 2
		if(arg1 == MiraMod_GetTranslation("Arion_Phase2")) then
			MiraMod_PlaySound("phase2");
			MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Phase2"), 1.0, 0.32, 0.66);
		end
	end

	if(arg2 == MiraMod_GetTranslation("Elementium_Monstrosity")) then
		-- Phase 3
		if(arg1 == MiraMod_GetTranslation("Elementium_Monstrosity_Phase3")) then
			MiraMod_PlaySound("phase3");
			MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Phase3"), 1.0, 0.32, 0.66);
			MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("10m_Range"), 1.0, 0.32, 0.66);
		end
	end

	if(arg2 == MiraMod_GetTranslation("Chogall")) then

			-- Phase 2
			if(arg1 == MiraMod_GetTranslation("Chogall_Phase2")) then
				MiraMod_PlaySound("phase2");
				MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Phase2"), 1.0, 0.32, 0.66);

			end

	end
end

function MiraMod_OnTerrastraCastBeben()

	MiraMod_AddSpecialTimer(time() + 6, MiraMod_GetTranslation("TerrastraCastBeben"), "beben", nil);

end

function MiraMod_OnArionCastDonnerschock()

	MiraMod_AddSpecialTimer(time() + 6, MiraMod_GetTranslation("ArionCastDonnerschock"), "donnerschock", nil);

end
