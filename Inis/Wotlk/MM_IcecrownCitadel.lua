function MiraMod_IcecrownCitadel_Init(ini)

	-- Singdragosa Stuff
	oldraid_symbol 						= 0;
	Phase2_IceBlockCounter 				= 0;
	Phase2 								= false;

	-- Debuffs
	-- ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)

	-- Nekrotische Seuche (Lich King, ICC)
	ini:addDebuff("Necrotic Plague", "seuche", "Plague");

	-- Schattengef�ngnis (Blutprinzen, ICC)
	ini:addDebuff("BloodCouncil_Shadow_Prison", nil, nil, "verbindungweg", "BloodCouncil_Shadow_Prison_Rem");

	-- Entfesselte Seuche (Prof. Hero, ICC)
	ini:addDebuff("Putricide_Unbound_Plague", "seuche", "Putricide_Unbound_Plague_Add", "verbindungweg", "Putricide_Unbound_Plague_Rem", false, nil, nil, 0, false, 10);

	-- Seuchenkrankheit (Prof. Hero, ICC)
	ini:addDebuff("Putricide_Plague_Sickness", "lauf", "Putricide_Plague_Sickness_Add", "verbindungweg", "Putricide_Plague_Sickness_Rem");

	-- Entfesselte Magie (Sindragosa, ICC)
	ini:addDebuff("Sindragosa_Unleashed_Magic", "lauf", "Sindragosa_Unleashed_Magic_Add", "verbindungweg", "Sindragosa_Unleashed_Magic_Rem", false, nil, nil, 0, false, 28);

	-- Combat
	-- ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)

	-- Yells
	ini.onYell = MiraMod_IcecrownCitadel_Yell;

	-- Raid Target Update
	ini.onRaidTargetUpdate = MiraMod_IcecrownCitadel_RaidTargetUpdate;

end

function MiraMod_IcecrownCitadel_Yell(arg1, arg2)

	if(arg2 == MiraMod_GetTranslation("Sindragosa")) then

		-- Airphase
		if(arg1 == MiraMod_GetTranslation("Sindragosa_AirPhase")) then
			MiraMod_PlaySound("hebtab");
			MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("AirPhase"), 1.0, 0.32, 0.66);
		end

		-- Phase 2
		if(arg1 == MiraMod_GetTranslation("Sindragosa_Phase2")) then
			Phase2 = true;
			MiraMod_PlaySound("phase2");
			MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Phase2"), 1.0, 0.32, 0.66);
		end

		-- Pull
		if(arg1 == MiraMod_GetTranslation("Sindragosa_Pull")) then
			Phase2_IceBlockCounter = 0;
			Phase2 = false;

			MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Sindragosa_GoodLuck"), 1.0, 1.0, 0.55);
		end

	end

end

function MiraMod_IcecrownCitadel_RaidTargetUpdate()

	local subzone	= GetSubZoneText();

	if (subzone == MiraMod_GetTranslation("Sindragosa_Zone")) then

		local index = GetRaidTargetIndex("player");

		local someonehasskull = false;

		if(Phase2 == true) then
			local numplayers = GetNumGroupMembers();

			for za=1,numplayers do
				local name = GetRaidRosterInfo(za);

				local icon = GetRaidTargetIndex(name);

				if(icon) then
					if(icon==8) then
							someonehasskull = true;
							break;
					end
				end
			end

			if(someonehasskull == true) then
				Phase2_IceBlockCounter = Phase2_IceBlockCounter+1;
				DEFAULT_CHAT_FRAME:AddMessage("Iceblockcounter: "..Phase2_IceBlockCounter, 0.5, 0.5, 1.0);
			end
		end

		if(index ~= oldraid_symbol) then

			local Debuff 	= UnitDebuff("player", MiraMod_GetTranslation("Sindragosa_FrostBeacon"));
			local InCombat 	= InCombatLockdown();

			-- Should only fire when the debuff is set, to prevent false alarms OR when we're not in combat for test reasons
			if(Debuff or InCombat == nil) then

				oldraid_symbol = index;

				if(Phase2 == false) then

					local InstanceDifficulty = GetInstanceDifficulty();

					if(InstanceDifficulty==2 or InstanceDifficulty==4) then

						-- 25 man modes

						-- vorne links		VIERECK
						if(index == 6) then
							MiraMod_PlaySound("vornelinks");
							MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Sindragosa_Front_Left"), 1.0, 1.0, 0.55);
						end

						-- vorne rechts		MOND
						if(index == 5) then
							MiraMod_PlaySound("vornerechts");
							MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Sindragosa_Front_Right"), 1.0, 1.0, 0.55);
						end

						-- mitte			KREUZ + DIAMANT
						if(index == 7 or index == 3) then
							MiraMod_PlaySound("mitte");
							MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Sindragosa_Center"), 1.0, 1.0, 0.55);
						end

						-- hinten links		DREIECK
						if(index == 4) then
							MiraMod_PlaySound("hintenlinks");
							MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Sindragosa_Rear_Left"), 1.0, 1.0, 0.55);
						end

						-- hinten rechts	KOPF
						if(index == 8) then
							MiraMod_PlaySound("hintenrechts");
							MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Sindragosa_Rear_Right"), 1.0, 1.0, 0.55);
						end

					end

					if(InstanceDifficulty==1 or InstanceDifficulty==3) then

						-- 10 man modes

						-- vorne links		KREUZ
						if(index == 7) then
							MiraMod_PlaySound("vornelinks");
							MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Sindragosa_Front_Left"), 1.0, 1.0, 0.55);
						end

						-- vorne rechts		KOPF
						if(index == 8) then
							MiraMod_PlaySound("vornerechts");
							MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Sindragosa_Front_Right"), 1.0, 1.0, 0.55);
						end

					end

				end

				if(Phase2 == true) then
					-- code here for phase 2
					if(index == 8) then

						if(Phase2_IceBlockCounter % 2 == 1) then
							MiraMod_PlaySound("vornelinks");
							MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Sindragosa_Front_Left"), 1.0, 1.0, 0.55);
						end

						if(Phase2_IceBlockCounter % 2 == 0) then
							MiraMod_PlaySound("vornerechts");
							MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Sindragosa_Front_Right"), 1.0, 1.0, 0.55);
						end

					end

				end

			end

		else
			oldraid_symbol = 0;
		end

	end

end
