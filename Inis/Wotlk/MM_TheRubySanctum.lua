function MiraMod_RubySanctum_Init(ini)
	
	-- Debuffs
	-- ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)

	-- Feurige Einäscherung (Halion, Rubinsanktum)
	ini:addDebuff("Halion_Fiery_Combustion", "lauf", "Halion_Fiery_Combustion_Add", nil, "Halion_Fiery_Combustion_Rem");

	-- Seelenverzehrung (Halion, Rubinsanktum)
	ini:addDebuff("Halion_Soul_Consumption", "lauf", "Halion_Soul_Consumption_Add", nil, "Halion_Soul_Consumption_Rem");

	-- Combat
	-- ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)

end
