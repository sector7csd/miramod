function MiraMod_HellfireCitadel_Init(ini)

    local playerName = GetUnitName("player", false);

    -- Debuffs
    -- ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)
    ini:addDebuff("Gorefiend_Digest", "hebtab", "Gorefiend_Digest_Add", "verbindungweg", "Gorefiend_Digest_Rem", false, nil, nil, nil, true);
    ini:addDebuff("Gorefiend_Touch_of_Doom", "seuche", "Gorefiend_Touch_of_Doom_Add", "verbindungweg", "Gorefiend_Touch_of_Doom_Rem");
    ini:addDebuff("Gorefiend_Shared_Fate", nil, nil, "verbindungweg", "Gorefiend_Shared_Rem");
    ini:addDebuff("TyrantVelhari_Font_of_Corruption", "seuche", "TyrantVelhari_Font_of_Corruption_Add", "verbindungweg", "TyrantVelhari_Font_of_Corruption_Rem", false, nil, nil, nil, true);
    ini:addDebuff("Xhulhorac_Fel_Surge", "green", "Xhulhorac_Fel_Surge_Add", "verbindungweg", "Xhulhorac_Fel_Surge_Rem", false, nil, nil, nil, true);
    ini:addDebuff("Xhulhorac_Void_Surge", "purple", "Xhulhorac_Void_Surge_Add", "verbindungweg", "Xhulhorac_Void_Surge_Rem", false, nil, nil, nil, true);
    ini:addDebuff("Socrethar_Gift_Manari", "seuche", "Socrethar_Gift_Manari_Add", "verbindungweg", "Socrethar_Gift_Manari_Rem", false, nil, nil, nil, true);

    -- Combat
    -- ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)
    ini:addCombat(nil, "SPELL_AURA_APPLIED", 179909, nil, "gouge",          "Gorefiend_Shared_Add", MiraMod_onBlockingSharedFate, nil, playerName);
    ini:addCombat(nil, "SPELL_AURA_APPLIED", 179908, nil, "hinlauf",        "Gorefiend_Shared_Add", MiraMod_onSharedFate, nil, playerName);
    ini:addCombat("TyrantVelhari", "SPELL_AURA_APPLIED", 179986, "TyrantVelhari_Aura_of_Contempt", "phase2", TyrantVelhari_Aura_of_Contempt_Msg, nil,  nil, playerName);
    ini:addCombat("Kilrogg_Deadeye_Salivating_Bloodthirster", "SPELL_AURA_APPLIED", 182153, "Kilrogg_Deadeye_Fel_Claws", "newadd", "NewAdd");

    ini.onStartEncounter = MiraMod_HellfireCitadel_onStartEncounter;

end

function MiraMod_HellfireCitadel_onStartEncounter(encounterId, encounterName, diff, players, ini)

end

function MiraMod_onSharedFate(ini)

    SendChatMessage(MiraMod_GetTranslation("Gorefiend_Shared_HaveToRun"), "YELL", nil, 0);

end

function MiraMod_onBlockingSharedFate(ini)

    SendChatMessage(MiraMod_GetTranslation("Gorefiend_Shared_RunToMe"), "YELL", nil, 0);

end
