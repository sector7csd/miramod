function MiraMod_BlackrockFoundry_Init(ini)

	-- Debuffs
	-- ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown)

	-- Combat
	-- ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)
	ini:addCombat("Blast_Furnace_Primal_Elementalist", 	"SPELL_AURA_APPLIED", 158345, "Blast_Furnace_Primal_Elementalist_Schilds_Down", "verbindung", "Blast_Furnace_Primal_Elementalist_Schilds_Down_Msg");
	ini:addCombat("Blast_Furnace_Primal_Elementalist", 	"SPELL_AURA_REMOVED", 158345, "Blast_Furnace_Primal_Elementalist_Schilds_Down", nil, nil, MiraMod_BlackrockFoundry_onPrimalElementalistSchildsUp);
	ini:addCombat("Blast_Furnace_Bellows_Operator",    	"SPELL_CAST_SUCCESS", 155181, "Blast_Furnace_Loading", nil, nil, MiraMod_BlackrockFoundry_onBlastFurnaceLoading);
	ini:addCombat("Blast_Furnace_Heat_Regulator",		"UNIT_DIED",          nil,    nil,                     nil, nil, MiraMod_BlackrockFoundry_onBlastFurnaceHeatRegulatorDied);
	ini:addCombat("Blast_Furnace_Primal_Elementalist",	"UNIT_DIED",		  nil,	  nil,					   "adddown", "AddDown", MiraMod_BlackrockFoundry_onPrimalElementalistDied);

	ini.onStartEncounter = MiraMod_BlackrockFoundry_onStartEncounter;

end

function MiraMod_BlackrockFoundry_onStartEncounter(encounterId, encounterName, diff, players, ini)

	if(MiraMod_GetTranslation("Blast_Furnace") == encounterName) then

		ini.blast_Furnace_ignoreLoading 		= true;
		ini.blast_Furnace_Heat_Regulators 		= 2;
		ini.blast_Furnace_Primal_Elementalists  = 4;

		-- MiraMod_AddSpecialTimer(EndTime, Msg, Snd, Function, Name)
		MiraMod_AddSpecialTimer(time() + 3, nil, nil, MiraMod_BlackrockFoundry_onBlastFurnaceIgnoreReset);
		MiraMod_AddSpecialTimer(time() + 50, MiraMod_GetTranslation("NewAddSoon"), "newaddsoon", nil, "BlastFurnaceNewAdds");

	end

end

function MiraMod_BlackrockFoundry_onBlastFurnaceIgnoreReset(ini)

	ini.blast_Furnace_ignoreLoading = false;

end

function MiraMod_BlackrockFoundry_onBlastFurnaceLoading(ini)

	if(ini.blast_Furnace_ignoreLoading == false) then

		MiraMod_PlaySound("newadd");
		MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("NewAdd"), 1.0, 0.32, 0.66);

	end

end

function MiraMod_BlackrockFoundry_onBlastFurnaceHeatRegulatorDied(ini)

	ini.blast_Furnace_Heat_Regulators = ini.blast_Furnace_Heat_Regulators -1;
	MiraMod_Write("Remaining Heat regulators: " .. ini.blast_Furnace_Heat_Regulators);

	if(ini.blast_Furnace_Heat_Regulators == 0) then

		MiraMod_Write("Canceling new adds warning timer");
		MiraMod_CancelSpecialTimer("BlastFurnaceNewAdds");

	end

end

function MiraMod_BlackrockFoundry_onPrimalElementalistSchildsUp(ini)

	MiraMod_AddSpecialTimer(time() + 1, nil, nil, MiraMod_BlackrockFoundry_AnnounceSchildUp, "BlastFurnaceSchildsUp");

end

function MiraMod_BlackrockFoundry_onPrimalElementalistDied(ini)

	MiraMod_CancelSpecialTimer("BlastFurnaceSchildsUp");

	ini.blast_Furnace_Primal_Elementalists = ini.blast_Furnace_Primal_Elementalists -1;
	MiraMod_Write("Remaining Primal Elementalists: " .. ini.blast_Furnace_Primal_Elementalists);

end

function MiraMod_BlackrockFoundry_AnnounceSchildUp(ini)

	MiraMod_PlaySound("verbindungweg");
	MiraModTextDisplay:AddMessage(MiraMod_GetTranslation("Blast_Furnace_Primal_Elementalist_Schilds_Down_Rem"), 1.0, 0.32, 0.66);

end
