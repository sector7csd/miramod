function MiraMod_Highmaul_Init(ini)
		
	local playerName = GetUnitName("player", false);

	-- function ini:addCombat(source, actionType, spellId, spellName, sound, textMsg, func, stackCount, target)
	ini:addCombat("Imperator_Margok", 	"SPELL_CAST_START",		157349, "Imperator_Margok_Force_Nova",  						"seuche", 	"Imperator_Margok_Force_Nova_Msg");
	ini:addCombat("Imperator_Margok", 	"SPELL_CAST_START",		164232, "Imperator_Margok_Force_Nova_Displacement", 			"seuche", 	"Imperator_Margok_Force_Nova_Msg");
	ini:addCombat("Imperator_Margok", 	"SPELL_CAST_START",		164235, "Imperator_Margok_Force_Nova_Fortification",			"seuche", 	"Imperator_Margok_Force_Nova_Msg");
	ini:addCombat("Imperator_Margok", 	"SPELL_CAST_START",		0, 		"Imperator_Margok_Force_Nova_Replication", 				"seuche", 	"Imperator_Margok_Force_Nova_Msg");

	ini:addCombat("Imperator_Margok", 	"SPELL_CAST_START",		156467, "Imperator_Margok_Destructive_Resonance", 				"lauf", 	"Imperator_Margok_Destructive_Resonance_Msg");
	ini:addCombat("Imperator_Margok", 	"SPELL_CAST_START",		164075, "Imperator_Margok_Destructive_Resonance_Displacement", 	"lauf", 	"Imperator_Margok_Destructive_Resonance_Msg");
	ini:addCombat("Imperator_Margok", 	"SPELL_CAST_START",		164076, "Imperator_Margok_Destructive_Resonance_Fortification", "lauf", 	"Imperator_Margok_Destructive_Resonance_Msg");
	ini:addCombat("Imperator_Margok", 	"SPELL_CAST_START",		0, 		"Imperator_Margok_Destructive_Resonance_Replication", 	"lauf", 	"Imperator_Margok_Destructive_Resonance_Msg");

	-- function ini:addDebuff(name, addSound, addTextMsg, removeSound, removeMsg, scanInGroup, addYell, removeYell, stackCount, tiggerCountdown, countDown, announceStackCount)
	ini:addDebuff("Imperator_Margok_Branded",  				nil, "Imperator_Margok_Branded_Msg",  "verbindungweg", "Imperator_Margok_Branded_Rem", false, nil, nil, nil, false, 0, true);
	ini:addDebuff("Imperator_Margok_Branded_Displacement",  nil, "Imperator_Margok_Branded_Msg",  "verbindungweg", "Imperator_Margok_Branded_Rem", false, nil, nil, nil, false, 0, true);
	ini:addDebuff("Imperator_Margok_Branded_Fortification", nil, "Imperator_Margok_Branded_Msg",  "verbindungweg", "Imperator_Margok_Branded_Rem", false, nil, nil, nil, false, 0, true);
	ini:addDebuff("Imperator_Margok_Branded_Replication",   nil, "Imperator_Margok_Branded_Msg",  "verbindungweg", "Imperator_Margok_Branded_Rem", false, nil, nil, nil, false, 0, true);

end
