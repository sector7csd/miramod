function MiraMod_CheckParam(Param)

	if(Param == nil) then
		return "-";
	end

	return Param;

end

function MiraMod_ClearTable(Table)

	local entries = table.getn(Table);
	for za=1, entries do
		table.remove(Table);
	end

end

function MiraMod_Write(msg, r, g, b)
	
	DEFAULT_CHAT_FRAME:AddMessage("[MiraMod] " .. msg, r, g, b);

end

function MiraMod_BoolToString(bool)

	if(bool == true) then
		return "true";
	end

	return "false";
end
