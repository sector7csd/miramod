local _, L = ...;
if GetLocale() == "enGB" or GetLocale() == "enUS" then

	SoundDir = "Interface\\AddOns\\MiraMod\\Sounds\\en";

	-- Common messages
	L["StartUpMsg"] 							= "MiraMod loaded and ready...";
	L["Phase2"]									= ">> Phase 2 <<";
	L["Phase3"]									= ">> Phase 3 <<";
	L["AirPhase"]								= ">> Airphase <<";
	L["AirPhase_Soon"]							= ">> Airphase soon <<";
	L["NextPhaseSoon"]							= ">> Next phase soon <<";
	L["10m_Range"]								= "Hold 10 m distance";
	L["prepared"]								= "prepared";
	L["active"]									= "active";

	L["NewAdd"]									= ">> New add <<";
	L["AddDown"]								= ">> Add down <<";
	L["NewAddSoon"]								= ">> New add soon <<";

	L["Shadowvortex"]							= "Shadow Vortex";
	L["Shadowvortex_Add"]						= ">> Shadow Vortex <<";
	L["Shadowvortex_Rem"]						= ">> Shadow Vortex removed <<";
	L["BubbleBound"]							= "Bubble Bound";
	L["BubbleBound_Add"]						= ">> Bubble Bound <<";
	L["BubbleBound_Rem"]						= ">> Bubble Bound removed <<";
	L["BubbleBound_Yell_Add"]					= ">> Bubble Bound on me ! <<";
	L["BubbleBound_Yell_Rem"]					= "<< Bubble Bound removed >>";

	-- Test
	L["Weakened Soul"]							= "Weakened Soul";
	L["Weakened Soul Get"]						= ">> Weakened Soul <<";
	L["Weakened Soul Rem"] 						= ">> Weakened Soul removed <<";
	L["Power Word Fortitude"]					= "Power Word: Fortitude";

	-- Instances
	L["BlackwingDescent"]						= "Blackwing Descent";
	L["TheBastionofTwilight"]					= "The Bastion of Twilight";
	L["IcecrownCitadel"]						= "Icecrown Citadel";
	L["Firelands"]								= "Firelands";
	L["MogushanVaults"]							= "Mogu'shan Vaults";
	L["ThroneoftheFourWinds"]					= "Throne of the Four Winds";
	L["HeartofFear"]							= "Heart of Fear";
	L["DragonSoul"]								= "Dragon Soul";
	L["TheRubySanctum"]							= "The Ruby Sanctum";
	L["SiegeofOrgrimmar"]						= "Siege of Orgrimmar";
	L["Highmaul"]								= "Highmaul";
	L["BlackrockFoundry"]						= "Blackrock Foundry";
	L["BlackrockCaverns"]						= "Blackrock Caverns";
	L["ShadowfangKeep"]							= "Shadowfang Keep";
	L["TheVortexPinnacle"]						= "TheVortexPinnacle";
	L["TheStonecore"]							= "The Stonecore";
	L["HallsofOrigination"]						= "Halls of Origination";
	L["HellfireCitadel"]						= "Hellfire Citadel";
	L["CourtofStars"]							= "CourtofStars";
	L["AssaultonVioletHold"]					= "Assault on Violet Hold";

	-- Sha of Pride
	L["ShaofPride"]								= "Sha of Pride";
	L["ShaofPride_GiftoftheTitans"]				= "Gift of the Titans";
	L["ShaofPride_GiftoftheTitans_Add"]			= ">> Gift of the Titans <<";
	L["ShaofPride_GiftoftheTitans_Rem"]			= ">> Gift of the Titans removed <<";
	L["ShaofPride_PoweroftheTitans"]			= "Power of the Titans";
	L["ShaofPride_PoweroftheTitans_Add"]		= ">> Power of the Titans <<";
	L["ShaofPride_PoweroftheTitans_Rem"]		= ">> Power of the Titans removed <<";
	L["ShaofPride_WeakenedResolve"]				= "Weakened Resolve";
	L["ShaofPride_WeakenedResolve_Add"]			= ">> Rift debuff <<";
	L["ShaofPride_WeakenedResolve_Rem"]			= ">> Rift debuff removed <<";
	L["ShaofPride_WeakenedResolve_Yell_Rem"]	= ">> Riss Debuff removed <<";
	L["ShaofPride_SwellingPride"]				= "Swelling Pride";
	L["ShaofPride_SwellingPride_Soon"]			= ">> Swelling Pride soon <<";
	L["ShaofPride_Projection"]					= "Projection";
	L["ShaofPride_Projection_Add"]				= ">> Projection <<";
	L["ShaofPride_Projection_Rem"]				= ">> Projection removed <<";
	L["ShaofPride_Banishment"]					= "Banishment";
	L["ShaofPride_Banishment_Add"]				= ">> Banishment <<";
	L["ShaofPride_Banishment_Rem"]				= ">> Banishment removed <<";

	-- Iron Juggernaut
	L["IronJuggernaut"]							= "Iron Juggernaut";
	L["IronJuggernaut_SeismicActivity"]			= "Seismic Activity";
	L["IronJuggernaut_ShockPulse"]				= "Shock Pulse";
	L["IronJuggernaut_ShockPulse_Msg"]			= ">> Shock Pulse in 3 secs <<";
	L["IronJuggernaut_CutterLaser"]				= "Cutter Laser";
	L["IronJuggernaut_CutterLaserTarget"]		= "Cutter Laser Target";
	L["IronJuggernaut_CutterLaserTarget_Msg"]	= ">> Cutter Laser Target <<";

	-- Norushen
	L["Norushen"]								= "Norushen";
	L["Norushen_StopIt"]						= "You... must... contain... it...";

	-- Fallen_Protectors
	L["Fallen_Protectors_He_Softfoot"]			= "He_Softfoot";
	L["Fallen_Protectors_Gouge"]				= "Gouge";
	L["Fallen_Protectors_Gouge_Msg"]			= ">> Gouge <<";

	-- ICC Stuff
	L["Necrotic Plague"]		 				= "Necrotic Plague";
	L["Plague"]									= ">> Plague <<";

	-- Sindragosa
	L["Sindragosa"]								= "Sindragosa";
	L["Sindragosa_Pull"]						= "You are fools to have come to this place! The icy winds of Northrend will consume your souls!";
	L["Sindragosa_AirPhase"]					= "Your incursion ends here! None shall survive!";
	L["Sindragosa_Phase2"]						= "Now feel my master's limitless power and despair!";
	L["Sindragosa_GoodLuck"]					= "Sindragosa pulled - Good luck !";
	L["Sindragosa_Zone"]						= "The Frost Queen's Lair";
	L["Sindragosa_Front_Left"]					= "FRONT LEFT";
	L["Sindragosa_Front_Right"]					= "FRONT RIGHT";
	L["Sindragosa_Center"]						= "CENTER";
	L["Sindragosa_Rear_Left"]					= "REAR LEFT";
	L["Sindragosa_Rear_Right"]					= "REAR RIGHT";
	L["Sindragosa_FrostBeacon"]					= "Frost Beacon";
	L["Sindragosa_Unleashed_Magic"]				= "Unleashed Magic";
	L["Sindragosa_Unleashed_Magic_Add"]			= ">> Unleashed Magic <<";
	L["Sindragosa_Unleashed_Magic_Rem"]			= ">> Unleashed Magic removed <<";

	-- Prof Putricide
	L["Putricide_Plague_Sickness"]				= "Plague Sickness";
	L["Putricide_Plague_Sickness_Add"]			= ">> Sickness <<";
	L["Putricide_Plague_Sickness_Rem"]			= ">> Sickness removed <<";
	L["Putricide_Unbound_Plague"]				= "Unbound Plague";
	L["Putricide_Unbound_Plague_Add"]			= ">> Plague <<";
	L["Putricide_Unbound_Plague_Rem"]			= ">> Plague removed <<";

	-- Blood Prince Council
	L["BloodCouncil_Shadow_Prison"]				= "Shadow Prison";
	L["BloodCouncil_Shadow_Prison_Rem"]			= ">> Shadow Prison removed <<";

	-- Halion
	L["Halion_Fiery_Combustion"]				= "Fiery Combustion";
	L["Halion_Fiery_Combustion_Add"]			= ">> Fiery Combustion <<";
	L["Halion_Fiery_Combustion_Rem"]			= ">> Fiery Combustion removed <<";
	L["Halion_Soul_Consumption"]				= "Soul Consumption";
	L["Halion_Soul_Consumption_Add"]			= ">> Soul Consumption <<";
	L["Halion_Soul_Consumption_Rem"]			= ">> Soul Consumption removed <<";

	-- Ozruk
	L["Ozruk"]									= "Ozruk";
	L["Ozruk_Reflect"]							= "Break yourselves upon my body. Feel the strength of the earth!";
	L["Ozruk_Reflect_Msg"]						= ">> Set dot's now <<";

	-- Maloriak
	L["Maloriak"]								= "Maloriak";
	L["Maloriak_Blue_Phase"]					= "How well does the mortal shell handle extreme temperature change? Must find out! For Science!";
	L["Maloriak_Blue_Phase_Msg"]				= ">> Blue Phase <<";
	L["Maloriak_Green_Phase"]					= "This one's a little unstable! But what's progress without failure?";
	L["Maloriak_Green_Phase_Msg"]				= ">> Green Phase <<";
	L["Maloriak_Red_Phase"]						= "Mix and stir! Apply heat!";
	L["Maloriak_Red_Phase_Msg"]					= ">> Red Phase <<";
	L["Maloriak_Low_HP_1"]						= "When pushed to the edge, results may become unpredictable!";
	L["Maloriak_Low_HP_2"]						= "Meet the brawn to my brains! Prime subjects, devour them!";
	L["Maloriak_Slime"]							= "Debilitating Slime";
	L["Maloriak_Slime_Add"]						= ">> Debilitating Slime <<";
	L["Maloriak_Slime_Rem"]						= ">> Debilitating Slime removed <<";
	L["Maloriak_Freeze"]						= "Flash Freeze";
	L["Maloriak_Freeze_Add"]					= ">> Flash Freeze <<";
	L["Maloriak_Freeze_Rem"]					= ">> Flash Freeze removed <<";
	L["Maloriak_Freeze_Yell_Add"]				= ">> Flash Freeze on me ! <<";
	L["Maloriak_Freeze_Yell_Rem"]				= "<< Flash Freeze removed >>";
	L["Maloriak_Flames"]						= "Consuming Flames";
	L["Maloriak_Flames_Add"]					= ">> Consuming Flames <<";
	L["Maloriak_Flames_Rem"]					= ">> Consuming Flames removed <<";
	L["Maloriak_Storm"]							= "Arcane Storm";
	L["Maloriak_Storm_Msg"]						= ">> Arcane Storm <<";

	-- Chimaeron
	L["Chimaeron"]								= "Chimaeron";
	L["Chimaeron_Feud"]							= "Feud";
	L["Chimaeron_Feud_Msg"]						= ">> Feud, run together <<";
	L["Chimaeron_DoubleAttack"]					= "Double Attack";
	L["Chimaeron_DoubleAttack_Msg"]				= ">> Double Attack <<";
	L["Chimaeron_Mortality"]					= "Mortality";

	-- Atramedes
	L["Atramedes"]								= "Atramedes";
	L["Atramedes_Pull"]							= "I have no need for eyes to see my enemies. Your clumsy footsteps and foul stench give you away!";
	L["Atramedes_Flame"]						= "You cannot hide from Searing Flame!";
	L["Atramedes_AirPhase"]						= "Yes, run! With every step your heartbeat quickens. The beating... loud and thunderous, almost deafening! You cannot escape!";
	L["Atramedes_Flame_Soon_Msg"]				= ">> Searing flame soon <<";
	L["Atramedes_Flame_Msg"]					= ">> Searing flame <<";
	L["Atramedes_Flame"]						= "Searing Flame";
	L["Atramedes_Execution_Sentence"]			= "Execution Sentence";
	L["Atramedes_Execution_Sentence_Add"]		= ">> Execution Sentence <<";
	L["Atramedes_Execution_Sentence_Rem"]		= ">> Execution Sentence removed <<";
	L["Atramedes_Execution_Sentence_Yell_Add"]	= ">> Execution Sentence on me ! <<";
	L["Atramedes_Execution_Sentence_Yell_Rem"]	= "<< Execution Sentence removed >>";
	L["Atramedes_Obnoxious_Fiend"]				= "Obnoxious Fiend";
	L["Atramedes_Obnoxious_Fiend_PhaseShift"]	= "Phase Shift";

	-- Halfus
	L["Halfus_DarkPool"]						= "Dark Pool";
	L["Halfus_DarkPool_Add"]					= ">> Dark Pool <<";
	L["Halfus_DarkPool_Rem"]					= ">> Dark Pool removed <<";

	-- Valiona
	L["Valiona"]								= "Valiona";
	L["Valiona_Flames"]							= "Devouring Flames";
	L["Valiona_Flames_Msg"]						= ">> Devouring Flames <<";
	L["Valiona_Twilight_Meteorite"]				= "Twilight Meteorite";
	L["Valiona_Twilight_Meteorite_Add"]			= ">> Meteorite, run together <<";
	L["Valiona_Twilight_Meteorite_Rem"]			= ">> Meteorite removed, run back <<";
	L["Valiona_Blackout"]						= "Blackout";
	L["Valiona_Blackout_Add"]					= ">> Blackout, run together <<";
	L["Valiona_Blackout_Rem"]					= ">> Blackout removed, run back <<";
	L["Valiona_Engulfing_Magic"]				= "Engulfing Magic";
	L["Valiona_Engulfing_Magic_Add"]			= ">> Engulfing Magic <<";
	L["Valiona_Engulfing_Magic_Rem"]			= ">> Engulfing Magic removed <<";

	-- Arion
	L["Arion"]									= "Arion";
	L["Arion_Phase2"]							= "Enough of this foolishness!";
	L["Arion_Thundershock"]						= "Thundershock";
	L["Arion_Winds"]							= "Swirling Winds";
	L["Arion_Winds_Add"]						= ">> Airbuff <<";
	L["Arion_Lightning_Rod"]					= "Lightning Rod";
	L["Arion_Lightning_Rod_Add"]				= ">> Lightning Rod <<";
	L["Arion_Lightning_Rod_Rem"]				= ">> Lightning Rod removed <<";

	-- Terrastra
	L["Terrastra"]								= "Terrastra";
	L["Terrastra_Quake"]						= "Quake";
	L["Terrastra_Grounded"]						= "Grounded";
	L["Terrastra_Grounded_Add"]					= ">> Earthbuff <<";

	L["TerrastraCastBeben"]						= ">> Take the earth buff <<";
	L["ArionCastDonnerschock"]					= ">> Take the air buff <<";

	-- Feludius
	L["Feludius_Waterlogged"]					= "Waterlogged";
	L["Feludius_Waterlogged_Add"]				= ">> Waterlogged -> Flameline <<";
	L["Feludius_Waterlogged_Rem"]				= ">> Waterlogged removed <<";

	-- Elementium Monstrosity
	L["Elementium_Monstrosity"]					= "Elementium Monstrosity";
	L["Elementium_Monstrosity_Phase3"]			= "BEHOLD YOUR DOOM!";

	-- Chogall
	L["Chogall"]								= "Cho'gall";
	L["Chogall_Worshipping"]					= "Worshipping";
	L["Chogall_Worshipping_Add"]				= ">> Worshipping on you <<";
	L["Chogall_Worshipping_Yell_Add"]			= ">> Worshipping on me ! <<";
	L["Chogall_Worshipping_Yell_Rem"]			= "<< Worshipping removed >>";
	L["Chogall_Corruption_Sickness"]			= "Corruption: Sickness";
	L["Chogall_Corruption_Sickness_Add"]		= "Sickness on you";
	L["Chogall_Corruption_Sickness_Yell_Add"]	= ">> Sickness on me ! <<";
	L["Chogall_Creates_Add"]					= "Summon Corrupting Adherent";
	L["Chogall_Corrupting_Adherent"]			= "Corrupting Adherent";
	L["Chogall_Phase2"]							= "Brother... it cannot be... (Destiny has come!) My strength... (Too proud! He speaks in blood! Listen!) My mind... (It is for His glory we serve!) What... is happening...? (Gone is the dawn. Come shades of twilight! Come madness! Come Havoc! Come infinite night!)";

	-- Anshal
	L["Anshal"]									= "Anshal";
	L["Anshal_Pull"]							= "It shall be I that earns the favor of our lord by casting out the intruders. My calmest wind shall prove too much for them!";

	-- Nezir
	L["Nezir"]									= "Nezir";
	L["Nezir_Pull"]								= "The honor of slaying the interlopers shall be mine, brothers! Their feeble bodies will freeze solid from my wind's icy chill!";

	-- Rohash
	L["Rohash"]									= "Rohash";
	L["Rohash_Ultimate"]						= "The power of our winds, UNLEASHED!";

	-- Asaad
	L["Asaad"]									= "Asaad";
	L["Asaad_Static_Cling"]						= "Static Cling";
	L["Asaad_Static_Cling_Msg"]					= ">> Static cling, jump <<";

	-- Al'Akir
	L["AlAkir"]									= "Al'Akir";
	L["AlAkir_Pull"]							= "Your challenge is accepted, mortals! Know that you face Al'Akir, Elemental Lord of Air! You have no hope of defeating me!";
	L["AlAkir_Phase2"]							= "Your futile persistence angers me.";
	L["AlAkir_WindBurst"]						= "Wind Burst";
	L["AlAkir_WindBrust_Soon_Msg"]				= ">> Windburst soon <<";
	L["AlAkir_WindBrust_Msg"]					= ">> Windburst <<";

	-- Omnotron
	L["Omnotron_Fixate"]						= "Fixate";
	L["Omnotron_Fixate_Add"]					= ">> Fixate <<";
	L["Omnotron_Fixate_Rem"]					= ">> Fixate removed <<";
	L["Omnotron_Acquiring_Target"]				= "Acquiring Target";
	L["Omnotron_Acquiring_Target_Add"]			= ">> Acquiring Target <<";
	L["Omnotron_Acquiring_Target_Rem"]			= ">> Acquiring Target removed <<";
	L["Omnotron_Acquiring_Target_Yell_Add"]		= ">> Acquiring Target on me ! <<";
	L["Omnotron_Acquiring_Target_Yell_Rem"]		= "<< Acquiring Target removed >>";
	L["Omnotron_Lightning_Conductor"]			= "Lightning Conductor";
	L["Omnotron_Lightning_Conductor_Add"]		= ">> Lightning Conductor <<";
	L["Omnotron_Lightning_Conductor_Rem"]		= ">> Lightning Conductor removed <<";

	-- Corla
	L["Corla_Evolution"]						= "Evolution";
	L["Corla_Evolution_Rem"]					= ">> Evolution removed <<";

	-- Lord Rhyolith
	L["Lord_Rhyolith"]							= "Lord Rhyolith";
	L["Lord_Rhyolith_Phase2"]					= "\195\132onen habe ich ungest\195\182rt durchgeschlafen\226\128\166 jetzt das\226\128\166 Fleischlinge, Ihr werdet BRENNEN!";
	L["Lord_Rhyolith_Crater"]					= "Crater";
	L["Lord_Rhyolith_Magmaflow"]				= "Magma Flow";
	L["Lord_Rhyolith_Magmaflow_Msg"]			= ">> Magma Flow <<";

	-- Baleroc
	L["Baleroc"]								= "Baloroc";
	L["Baleroc_Torment"] 						= "Torment";
	L["Baleroc_Torment_Msg"] 					= ">> Many tormentstacks, swap <<";
	L["Baleroc_Decimation_Blade"]				= "Decimation Blade";
	L["Baleroc_Decimation_Blade_Msg"]			= ">> Decimation Blade <<";
	L["Baleroc_Inferno_Blade"]					= "Inferno Blade";
	L["Baleroc_Inferno_Blade_Msg"]				= ">> Inferno Blade <<";

	-- Majordomo Staghelm
	L["Staghelm_Searing_Seeds"] 				= "Searing Seeds";
	L["Staghelm_Searing_Seeds_Msg"]				= ">> Searing Seeds <<";

	-- Ragnaros
	L["Ragnaros"]								= "Ragnaros";
	L["Ragnaros_Sulfuras_Smash"]				= "Sulfuras Smash"
	L["Ragnaros_Sulfuras_Smash_Msg"]			= ">> Sulfuras Smash <<";

	-- Shannox
	L["Shannox_Rageface"]						= "Rageface";
	L["Shannox_Wary"]							= "Wary";
	L["Shannox_Feeding_Frenzy"]					= "Feeding Frenzy";
	L["Shannox_Crystal_Prison_Trap"]			= "Crystal Prison Trap";
	L["Shannox_Crystal_Prison_Trap_Effect"]		= "Crystal Prison Trap Effect";
	L["Shannox_Rageface_in_trap_Msg"]		    = ">> Rageface in trap <<";
	L["Shannox_Rageface_broke_out_Msg"]		    = ">> Rageface broke out <<";
	L["Shannox_Feeding_Frenzy_expired_Msg"]		= ">> Rageface feeding frenzy has expired <<";
	L["Shannox_Wary_Msg"]						= ">> Rageface trap possible <<";
	L["Shannox_Trap_Immune"]				    = ">> Rageface trap immune for 25 secs <<";
	L["Shannox_Feeding_10"]						= ">> Feeding Frenzy stack is high <<";
	L["Shannox_Feeding_20"]						= ">> Feeding Frenzy stack is very high <<";
	L["Shannox_Feeding_30"]						= ">> Feeding Frenzy stack is now deadly <<";
	L["Shannox"]								= "Shannox";
	L["Shannox_Pull_Yell"]						= "Aha! The interlopers... Kill them! EAT THEM!";

	-- Ultraxion
	L["Ultraxion_Fading_Light"]					= "Fading Light";
	L["Ultraxion_Fading_Light_Msg"]				= ">> Fading Light <<";

	-- Zon'ozz
	L["Zonozz_Disrupting_Shadows"]				= "Disrupting Shadows";
	L["Zonozz_Disrupting_Shadows_Msg"]			= ">> Disrupting Shadows <<";
	L["Zonozz_Disrupting_Shadows_Rem"]			= ">> Disrupting Shadows removed <<";

	-- The Stone Guard
	L["Stone_Guard_Amethyst_Guardian"]			= "Amethyst Guardian";
	L["Stone_Guard_Amethyst_Pool"]				= "Amethyst Pool";
	L["Stone_Guard_Amethyst_Pool_Msg"]			= ">> Amethyst Pool <<";
	L["Stone_Guard_Cobalt_Mine"]				= "Cobalt Mine";
	L["Stone_Guard_Cobalt_Mine_Blast"]			= "Cobalt Mine Blast";
	L["Stone_Guard_Cobalt_Mine_Msg"]			= ">> Cobalt Mine <<";

	-- Feng
	L["Feng_Wildfire_Spark"]					= "Wildfire Spark";
	L["Feng_Wildfire_Spark_Msg"]				= ">> Wildfire Spark <<"
	L["Feng_Wildfire_Spark_Rem"]				= ">> Wildfire Spark removed <<";

	-- Gara'jal the Spiritbinder
	L["Garajal_Voodoo_Doll"]					= "Voodoo Doll";
	L["Garajal_Voodoo_Doll_Msg"]				= ">> Voodoo Doll <<"
	L["Garajal_Voodoo_Doll_Rem"]				= ">> Voodoo Doll removed <<";
	L["Garajal_Troll_Rush"]						= "Troll Rush";
	L["Garajal_Troll_Rush_Msg"]					= ">> Troll Rush <<";
	L["Garajal_Troll_Rush_Rem"]					= ">> Troll Rush weg <<";
	L["Garajal_Revitalized_Spirit"]				= "Revitalized Spirit";
	L["Garajal_Revitalized_Spirit_Msg"] 		= ">> You are now able to exit the ghostworld <<";
	L["Garajal_Revitalized_Spirit_Yell"]		= ">> I'm able to leave the ghostworld <<";

	-- Vizier Zor'lok
	L["Vizier_Zorlok"]							= "Imperial Vizier Zor'lok";
	L["Vizier_Zorlok_Noise_Cancelling"]			= "Noise Cancelling";
	L["Vizier_Zorlok_Force_and_Verve"]			= "Force and Verve";
	L["Vizier_Zorlok_Attenuation"]				= "Attenuation";
	L["Vizier_Zorlok_Attenuation_Msg"]			= ">> Attenuation <<";
	L["Vizier_Zorlok_Pull"]						= "The divine chose us to give mortal voice to Her divine will. We are but the vessel that enacts Her will.";

	-- Imperator Mar'gok
	L["Imperator_Margok"]										= "Imperator Mar'gok";
	L["Imperator_Margok_Force_Nova"]							= "Force Nova";
	L["Imperator_Margok_Force_Nova_Displacement"]				= "Force Nova: Displacement";
	L["Imperator_Margok_Force_Nova_Fortification"]				= "Force Nova: Fortification";
	L["Imperator_Margok_Force_Nova_Replication"]				= "Force Nova: Replication";
	L["Imperator_Margok_Force_Nova_Msg"]						= ">> Force Nova, jump over it <<";

	L["Imperator_Margok_Destructive_Resonance"] 				= "Destructive Resonance";
	L["Imperator_Margok_Destructive_Resonance_Displacement"]	= "Destructive Resonance: Displacement";
	L["Imperator_Margok_Destructive_Resonance_Fortification"]	= "Destructive Resonance: Fortification";
	L["Imperator_Margok_Destructive_Resonance_Replication"]		= "Destructive Resonance: Replication";
	L["Imperator_Margok_Destructive_Resonance_Msg"] 			= ">> Destructive Resonance <<";

	L["Imperator_Margok_Branded"]								= "Branded";
	L["Imperator_Margok_Branded_Displacement"]					= "Branded: Displacement";
	L["Imperator_Margok_Branded_Fortification"]					= "Branded: Fortification";
	L["Imperator_Margok_Branded_Replication"]					= "Branded: Replication";
	L["Imperator_Margok_Branded_Msg"]							= ">> Branded <<";
	L["Imperator_Margok_Branded_Rem"]							= ">> Branded removed <<";

	-- Blast Furnace
	L["Blast_Furnace"]											= "Blast Furnace";
	L["Blast_Furnace_Bellows_Operator"]							= "Bellows Operator";
	L["Blast_Furnace_Heat_Regulator"]							= "Heat Regulator";
	L["Blast_Furnace_Loading"]									= "Loading";
	L["Blast_Furnace_Primal_Elementalist"]						= "Primal Elementalist";
	L["Blast_Furnace_Primal_Elementalist_Schilds_Down"]			= "Schilds Down";
	L["Blast_Furnace_Primal_Elementalist_Schilds_Down_Msg"]		= ">> Schilds down <<";
	L["Blast_Furnace_Primal_Elementalist_Schilds_Down_Rem"]		= ">> Schilds up <<";

	-- Gorefiend
	L["Gorefiend"]												= "Gorefiend";
	L["Gorefiend_Digest"]										= "Digest";
	L["Gorefiend_Digest_Add"]									= ">> Digest <<";
	L["Gorefiend_Digest_Rem"]									= ">> Digest removed <<";
	L["Gorefiend_Shared_Fate"]									= "Shared Fate";
	L["Gorefiend_Shared_Add"]									= ">> Shared Fate <<";
	L["Gorefiend_Shared_Rem"]									= ">> Shared Fate removed <<";
	L["Gorefiend_Shared_RunToMe"]								= "<< Run to me >>";
	L["Gorefiend_Shared_HaveToRun"]								= "> I have to run <";
	L["Gorefiend_Touch_of_Doom"]								= "Touch of Doom";
	L["Gorefiend_Touch_of_Doom_Add"]							= ">> Touch of Doom <<";
	L["Gorefiend_Touch_of_Doom_Rem"]							= ">> Touch of Doom removed <<";

	-- Tyrant Velhari
	L["TyrantVelhari"]											= "Tyrant Velhari";
	L["TyrantVelhari_Font_of_Corruption"]						= "Font of Corruption";
	L["TyrantVelhari_Font_of_Corruption_Add"]					= ">> Font of Corruption <<";
	L["TyrantVelhari_Font_of_Corruption_Rem"]					= ">> Font of Corruption removed <<";
	L["TyrantVelhari_Aura_of_Contempt"]							= "Aura of Contempt";
	L["TyrantVelhari_Aura_of_Contempt_Msg"]						= ">> Aura of Contempt <<";

	-- Xhul'horac
	L["Xhulhorac_Fel_Surge"]									= "Fel Surge"
	L["Xhulhorac_Fel_Surge_Add"]								= ">> Fel Surge <<"
	L["Xhulhorac_Fel_Surge_Rem"]								= ">> Fel Surge removed <<"
	L["Xhulhorac_Void_Surge"]									= "Void Surge"
	L["Xhulhorac_Void_Surge_Add"]								= ">> Void Surge <<"
	L["Xhulhorac_Void_Surge_Rem"]								= ">> Void Surge removed <<"

	-- Kilrogg Deadeye
	L["Kilrogg_Deadeye_Salivating_Bloodthirster"]				= "Salivating Bloodthirster";
	L["Kilrogg_Deadeye_Fel_Claws"]								= "Fel Claws";

	-- Socrethar
	L["Socrethar_Gift_Manari"]									= "Gift of the Man'ari";
	L["Socrethar_Gift_Manari_Add"]								= ">> Gift of the Man'ari <<";
	L["Socrethar_Gift_Manari_Rem"]								= ">> Gift of the Man'ari removed <<";

	-- Patrol Captain Gerdo
	L["CaptainGerdo_Arcane_Lockdown"]							= "Arcane Lockdown";
	L["CaptainGerdo_Arcane_Lockdown_Add"]						= ">> Arcane Lockdown: jump <<";
	L["CaptainGerdo_Arcane_Lockdown_Rem"]						= ">> Arcane Lockdown removed <<";

	-- Blood-Princess Thal'ena
	L["Thalena_Essence"]										= "Essence of the Blood Princess";

	L["Thalena_Frenzied_Bloodthirst"]							= "Frenzied Bloodthirst";
	L["Thalena_Frenzied_Bloodthirst_Add"]						= ">> Frenzied Bloodthirst: Bite now! <<";
	L["Thalena_Frenzied_Bloodthirst_Rem"]						= ">> Frenzied Bloodthirst removed <<";

end
